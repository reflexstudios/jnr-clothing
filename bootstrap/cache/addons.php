<?php return array (
  'aryehraber/statamic-captcha' => 
  array (
    'id' => 'aryehraber/statamic-captcha',
    'slug' => 'captcha',
    'editions' => 
    array (
    ),
    'marketplaceId' => 277,
    'marketplaceSlug' => 'captcha',
    'marketplaceUrl' => 'https://statamic.com/addons/aryeh-raber/captcha',
    'marketplaceSellerSlug' => 'aryeh-raber',
    'latestVersion' => '1.5.2',
    'version' => '1.5.2',
    'namespace' => 'AryehRaber\\Captcha',
    'autoload' => 'src',
    'provider' => 'AryehRaber\\Captcha\\CaptchaServiceProvider',
    'name' => 'Captcha',
    'url' => NULL,
    'description' => 'Protect your Statamic forms using a Captcha service',
    'developer' => 'Aryeh Raber',
    'developerUrl' => 'https://aryeh.dev',
    'email' => NULL,
  ),
  'aryehraber/statamic-uuid' => 
  array (
    'id' => 'aryehraber/statamic-uuid',
    'slug' => NULL,
    'editions' => 
    array (
    ),
    'marketplaceId' => 342,
    'marketplaceSlug' => 'uuid',
    'marketplaceUrl' => 'https://statamic.com/addons/aryeh-raber/uuid',
    'marketplaceSellerSlug' => 'aryeh-raber',
    'latestVersion' => '2.0.3',
    'version' => '2.0.3',
    'namespace' => 'AryehRaber\\Uuid',
    'autoload' => 'src',
    'provider' => 'AryehRaber\\Uuid\\UuidServiceProvider',
    'name' => 'Uuid',
    'url' => NULL,
    'description' => 'One of its kind; unlike anything else',
    'developer' => 'Aryeh Raber',
    'developerUrl' => 'https://aryeh.dev',
    'email' => NULL,
  ),
);