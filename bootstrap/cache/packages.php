<?php return array (
  'ajthinking/archetype' => 
  array (
    'aliases' => 
    array (
      'LaravelFile' => 'Archetype\\Facades\\LaravelFile',
      'PHPFile' => 'Archetype\\Facades\\PHPFile',
    ),
    'dont-discover' => 
    array (
    ),
    'providers' => 
    array (
      0 => 'Archetype\\ServiceProvider',
    ),
  ),
  'aryehraber/statamic-captcha' => 
  array (
    'providers' => 
    array (
      0 => 'AryehRaber\\Captcha\\CaptchaServiceProvider',
    ),
  ),
  'aryehraber/statamic-uuid' => 
  array (
    'providers' => 
    array (
      0 => 'AryehRaber\\Uuid\\UuidServiceProvider',
    ),
  ),
  'barryvdh/laravel-debugbar' => 
  array (
    'providers' => 
    array (
      0 => 'Barryvdh\\Debugbar\\ServiceProvider',
    ),
    'aliases' => 
    array (
      'Debugbar' => 'Barryvdh\\Debugbar\\Facade',
    ),
  ),
  'facade/ignition' => 
  array (
    'providers' => 
    array (
      0 => 'Facade\\Ignition\\IgnitionServiceProvider',
    ),
    'aliases' => 
    array (
      'Flare' => 'Facade\\Ignition\\Facades\\Flare',
    ),
  ),
  'fideloper/proxy' => 
  array (
    'providers' => 
    array (
      0 => 'Fideloper\\Proxy\\TrustedProxyServiceProvider',
    ),
  ),
  'fruitcake/laravel-cors' => 
  array (
    'providers' => 
    array (
      0 => 'Fruitcake\\Cors\\CorsServiceProvider',
    ),
  ),
  'intervention/image' => 
  array (
    'providers' => 
    array (
      0 => 'Intervention\\Image\\ImageServiceProvider',
    ),
    'aliases' => 
    array (
      'Image' => 'Intervention\\Image\\Facades\\Image',
    ),
  ),
  'laravel/tinker' => 
  array (
    'providers' => 
    array (
      0 => 'Laravel\\Tinker\\TinkerServiceProvider',
    ),
  ),
  'nesbot/carbon' => 
  array (
    'providers' => 
    array (
      0 => 'Carbon\\Laravel\\ServiceProvider',
    ),
  ),
  'nunomaduro/collision' => 
  array (
    'providers' => 
    array (
      0 => 'NunoMaduro\\Collision\\Adapters\\Laravel\\CollisionServiceProvider',
    ),
  ),
  'rebing/graphql-laravel' => 
  array (
    'providers' => 
    array (
      0 => 'Rebing\\GraphQL\\GraphQLServiceProvider',
    ),
    'aliases' => 
    array (
      'GraphQL' => 'Rebing\\GraphQL\\Support\\Facades\\GraphQL',
    ),
  ),
  'statamic/cms' => 
  array (
    'providers' => 
    array (
      0 => 'Statamic\\Providers\\StatamicServiceProvider',
    ),
    'aliases' => 
    array (
      'Statamic' => 'Statamic\\Statamic',
    ),
  ),
  'wilderborn/partyline' => 
  array (
    'providers' => 
    array (
      0 => 'Wilderborn\\Partyline\\ServiceProvider',
    ),
    'aliases' => 
    array (
      'Partyline' => 'Wilderborn\\Partyline\\Facade',
    ),
  ),
);