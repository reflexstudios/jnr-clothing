---
id: 1d567d19-8f2d-42f4-89f9-ba8a1e4d104f
blueprint: pages
title: Components
updated_by: 4bfa4be6-8be8-40f2-859e-bf8cbe8ba010
updated_at: 1646713029
white_background: false
content:
  -
    type: heading
    attrs:
      level: 2
    content:
      -
        type: text
        text: 'Testing The Content'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'We provide high quality logo printing and embroidery across our huge range of work clothing. Browse our range below and get in touch for a quote. We can personalise a wide range of clothing with your logo and branding.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Our range includes fleeces, softshells, sweatshirts and more, with versatile stock to suit all kinds of industry. Simply browse the range below and select the products you want to customise to create your own custom workwear. You can complete your order with personalisation online, or for larger purchases, contact us for a free no-obligation quote.'
  -
    type: set
    attrs:
      values:
        type: faqs
        faqs:
          -
            question: Question
            answer: 'Our range includes fleeces, softshells, sweatshirts and more, with versatile stock to suit all kinds of industry. Simply browse the range below and select the products you want to customise to create your own custom workwear. You can complete your order with personalisation online, or for larger purchases, contact us for a free no-obligation quote.'
          -
            question: 'atile stock to sses'
            answer: 'Our range includes fleeces, softshells, sweatshirts and more, with versatile stock to suit all kinds of industry. Simply browse the range below and select the products you want to customise to create your own custom workwear. You can complete your order with personalisation online, or for larger purchases, conta'
  -
    type: set
    attrs:
      values:
        type: product_cards
        product_cards_title: 'Highlighted products'
        products:
          - 34fb12ec-c993-4fd4-94fb-2ef740808b4d
          - 95099e98-766d-490a-a340-497c7130056c
          - ca88971c-0e8e-47ad-9985-5f1f38e50805
          - bba4b097-f85f-4b93-b486-63a0f60ee640
          - 74ab7adc-b36e-498e-ab9b-5e5dcb8fb3f6
          - 88d260c8-79eb-4879-8d8f-e1cd86481bed
          - 63c7d6f6-3971-44ac-bac5-d0ad5d5c6d8f
          - 6416f9c8-5d78-4fcb-a218-244272412b09
  -
    type: set
    attrs:
      values:
        type: get_a_quote_form
        get_a_quote_form_title: 'Get a Quote'
        get_a_quote_form_subtext: 'Our range includes fleeces, softshells, sweatshirts and more, with versatile stock to suit all kinds of industry. Simply browse the range below and select the products you want to customise to create your own custom workwear. You can complete your order with personalisation online, or for larger purchases, conta'
  -
    type: set
    attrs:
      values:
        type: testimonials
  -
    type: set
    attrs:
      values:
        type: cta_area
        cta_title: 'CTA Tite'
        cta_text: 'Our range includes fleeces, softshells, sweatshirts and more, with versatile stock to suit all kinds of industry. Simply browse the range below and select the products you want to customise to create your own custom workwear. You can complete your order with personalisation online, or for larger purchases, conta'
        cta_button_text: 'Get Something'
        cta_button_link: '#'
---
