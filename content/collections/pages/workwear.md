---
id: 3264eef4-cf4e-4da7-bcc1-894fd6bc4f84
blueprint: pages
template: default
title: Workwear
author: 45b2baf1-3b24-4a7e-9cfd-ee2c4a3aef1b
updated_by: 36032470-b7c6-4fda-8fbc-d8a75800f3c4
updated_at: 1646711584
white_background: false
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'We provide high quality logo printing and embroidery across our huge range of work clothing. Browse our range below and get in touch for a quote. We can personalise a wide range of clothing with your logo and branding.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Our range includes fleeces, softshells, sweatshirts and more, with versatile stock to suit all kinds of industry. Simply browse the range below and select the products you want to customise to create your own custom workwear. You can complete your order with personalisation online, or for larger purchases, contact us for a free no-obligation quote.'
  -
    type: set
    attrs:
      values:
        type: blocks
        add_block:
          -
            block_title: 'Order Samples'
            block_subtext: 'Visit our stores in Belfast or Lurgan to pick up a sample or fill in a enquiry form to receive your sample in the post'
            block_button_text: 'Order Sample'
            block_button_link: '#'
            block_icon: samples.svg
            type: add_block
            enabled: true
          -
            block_title: 'Bulk Orders'
            block_subtext: 'Looking to place a large order? Contact us directly and we’ll get back to you with an unbeatable price'
            block_button_text: 'Get in touch'
            block_button_link: '#'
            type: add_block
            enabled: true
            block_icon: bulk.svg
  -
    type: set
    attrs:
      values:
        type: product_cards
        product_cards_title: 'Browse Our Range'
        products:
          - 60ee6661-6977-41f5-9761-2eb2d3b85af1
          - 34fb12ec-c993-4fd4-94fb-2ef740808b4d
          - 8414a306-4656-41f2-b004-180effe7b23a
          - 26d8fe65-3c34-4dd2-91a2-943ef7eb90bf
  -
    type: paragraph
---
