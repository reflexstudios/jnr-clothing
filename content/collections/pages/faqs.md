---
id: 96f7ef0a-d032-4cbb-b55b-03ac589a298e
blueprint: pages
template: default
title: FAQs
author: 45b2baf1-3b24-4a7e-9cfd-ee2c4a3aef1b
updated_by: 4bfa4be6-8be8-40f2-859e-bf8cbe8ba010
updated_at: 1646701304
white_background: false
content:
  -
    type: set
    attrs:
      values:
        type: faqs
        faqs:
          -
            question: 'Customer Support'
            answer: 'Contact us via Phone (028)38346209, email customercare@jnrclothing.com'
          -
            question: 'How do I change or cancel an order after I''ve submitted it? '
            answer: 'Please contact us to confirm a return or amend/cancel your order here'
          -
            question: 'How long will it take my items to be processed? '
            answer: 'We aim to process all items the next working day and for items ordered before 10am the same working day. However during busier times this extends to 2-3 working days'
          -
            question: 'How do I get my items?'
            answer: 'CLICK AND COLLECT You can use our FREE click and collect service – just select local collection when you check out. The collection is available from JNR CLOTHING. We carry large stocks of items on this website. However, due to the wide range of size and colour combinations, some items may be temporarily out of stock. We will often send goods that are in stock straight away and send the outstanding goods as soon as we receive them from our suppliers. In the unlikely event of any item being out of stock and not expected to be delivered within the timeframe of the delivery option chosen, you will be notified by one of our customer service representatives and given the option either to continue with the rest of your order, substitute the unavailable items with similar alternatives, or cancel. We will be happy to discuss your options when we contact you. --DELIVERY-- You can also select to have your items delivered when you check out. We use Royal Mail 48hour delivery. Your parcel will be delivered by Royal Mail 2-5 days after being shipped. UK delivery is FREE for products totalling £50 or over. UK delivery costs £4.95 for products totalling under £50.'
          -
            question: 'How do I change my shipping address?'
            answer: 'Please contact us to arrange a change in shipping address and can change this over provided your item has not already been dispatched.'
          -
            question: 'How do I create an account?'
            answer: 'You create your account when checking out your items.'
---
