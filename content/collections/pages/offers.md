---
id: 817dae71-2ce4-47e5-a4d5-c9d32390f9a4
blueprint: pages
template: default
title: Offers
author: 45b2baf1-3b24-4a7e-9cfd-ee2c4a3aef1b
updated_by: 4bfa4be6-8be8-40f2-859e-bf8cbe8ba010
updated_at: 1646688513
content:
  -
    type: set
    attrs:
      values:
        type: product_cards
        products_card_title: test
        products:
          - c11c0747-6a92-4a30-a7e8-381be84e4442
          - 8414a306-4656-41f2-b004-180effe7b23a
          - 26d8fe65-3c34-4dd2-91a2-943ef7eb90bf
  -
    type: paragraph
---
