---
id: 374bf8c2-deb7-48eb-98d7-ec2dab620d92
blueprint: pages
template: default
title: Returns
author: 45b2baf1-3b24-4a7e-9cfd-ee2c4a3aef1b
updated_by: 4bfa4be6-8be8-40f2-859e-bf8cbe8ba010
updated_at: 1646701452
white_background: false
content:
  -
    type: heading
    attrs:
      level: 2
    content:
      -
        type: text
        text: Returns
  -
    type: paragraph
    content:
      -
        type: text
        text: 'You may return any item for a full refund of the goods* within 14 days of delivery. Goods must be in their original packaging with all labels and tags attached and unused. You can return to store with proof of purchase or via post. Pack your returns securely. We strongly advise you to obtain proof of posting if returning via post when returning items in case the parcel goes missing. We do not accept responsibility for any returns lost or damaged in transit.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: '* We do not refund postage and packaging charges.'
      -
        type: text
        text: "\_"
  -
    type: heading
    attrs:
      level: 2
    content:
      -
        type: text
        text: Exchanges
  -
    type: paragraph
    content:
      -
        type: text
        text: 'You can exchange items that don’t fit provided they are in their original packaging with all labels and tags attached and unused. You can do this in-store.'
  -
    type: paragraph
    content:
      -
        type: text
        text: "Please contact us and we will correct this as soon as possible. Please do not remove any tags/labels or discard any packaging if you know you have received the wrong item.\_"
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Return postal address is'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'JNR Clothing'
      -
        type: hard_break
      -
        type: text
        text: '3 North Street'
      -
        type: hard_break
      -
        type: text
        text: Lurgan
      -
        type: hard_break
      -
        type: text
        text: 'Bt67 9ag'
---
