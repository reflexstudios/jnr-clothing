---
id: 23c2c3ec-9d36-4254-97ba-f8b6e11e81d5
blueprint: pages
template: default
title: 'Terms and Conditions'
author: 45b2baf1-3b24-4a7e-9cfd-ee2c4a3aef1b
updated_by: 4bfa4be6-8be8-40f2-859e-bf8cbe8ba010
updated_at: 1646701722
white_background: false
content:
  -
    type: heading
    attrs:
      level: 2
    content:
      -
        type: text
        text: Returns
  -
    type: paragraph
    content:
      -
        type: text
        text: 'You may return any item for a full refund of the goods* within 14 days of delivery. Goods must be in their original packaging with all labels and tags attached and unused. You can return to the store with proof of purchase or via post. Pack your returns securely. We strongly advise you to obtain proof of posting if returning via post when returning items in case the parcel goes missing. We do not accept responsibility for any returns lost or damaged in transit.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: '* We do not refund postage and packaging charges.'
      -
        type: text
        text: "\_"
  -
    type: paragraph
---
