---
id: 7807f591-bfb4-42e6-af3c-90c9eebcf9df
blueprint: home_page
title: Home
template: home
author: 45b2baf1-3b24-4a7e-9cfd-ee2c4a3aef1b
updated_by: 36032470-b7c6-4fda-8fbc-d8a75800f3c4
updated_at: 1646712244
lhs_title: 'School Uniforms'
rhs_title: 'Custom Workwear'
lhs_button_text: 'Shop Uniforms'
lhs_button_link: 'entry::cf01712e-4922-4002-a1b0-18fba82dbb14'
rhs_button_text: 'Shop Workwear'
rhs_button_link: 'entry::3264eef4-cf4e-4da7-bcc1-894fd6bc4f84'
lhs_image:
  - School-Uniforms1.png
rhs_image:
  - images/workwareMan.png
item_1_text: 'Affordable School and Staff Uniforms'
item_2_text: 'Personalised clothing for sports and leisure'
item_3_text: 'Own logo uniforms for businesses'
item_1_icon: strong.svg
item_2_icon: warm.svg
item_3_icon: breathable.svg
cta_title: 'Are you a retailer?'
cta_text: 'We cater for retailers and schools as wholesale distributors. Get in touch to find out more.'
cta_button_text: 'Wholesale Enquiry'
cta_button_link: 'entry::af34459c-0d24-4402-886e-aa7234ac7c56'
---
