---
id: d7148cdf-90a6-42cc-9394-91e6485dfd0e
blueprint: pages
template: default
title: 'Caring for your Clothing'
author: 45b2baf1-3b24-4a7e-9cfd-ee2c4a3aef1b
updated_by: 4bfa4be6-8be8-40f2-859e-bf8cbe8ba010
updated_at: 1646700228
white_background: false
content:
  -
    type: heading
    attrs:
      level: 2
    content:
      -
        type: text
        text: 'General Garment Care'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Please always follow the manufacturer''s washing instructions on the garment label.'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'We recommend washing garments inside out.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Always wash with garments of similar colour and fabric content'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Allowing garments to dry naturally is always best and tumble drying should only be done strictly following the temperature guideline on the garment care label. Pleated garments should never be tumble dried.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'The use of fabric conditioners should be avoided as this can weaken fabrics contributing to pilling.'
  -
    type: heading
    attrs:
      level: 2
    content:
      -
        type: text
        text: 'Ironing:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Follow instructions on the garment label.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Do not iron the ribs on the neck, cuffs and body of the garment.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Do not iron printed areas.'
  -
    type: heading
    attrs:
      level: 2
    content:
      -
        type: text
        text: 'Sweatshirts and Knitwear'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Always wash and dry the garment using the instructions on the care label.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Always was with garments of similar colour and fabric content.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'The use of a fabric conditioner is not recommended.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Always wash the garment inside out and preferably wash before wearing first time to wash loose fibres away Follow instructions on the garment label.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Do not iron the ribs on the neck, cuffs and body of the garment.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Do not iron printed areas.'
  -
    type: heading
    attrs:
      level: 2
    content:
      -
        type: text
        text: 'Bobbling/Pilling on Garments:'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Every knitted fabric can pill. It is caused by rubbing, from other fabrics or products (such as zips or buttons, backpacks and overgarments) or not adhering to the washing instructions.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'It is especially important on the first few washes to wash any garment inside out and at a low temperature to remove any transferring fibres. Also, avoid the use of fabric conditioners, harsh cleaners and bleaches as these can weaken fibres and induce pilling. It is preferable to line dry where possible.'
  -
    type: heading
    attrs:
      level: 2
    content:
      -
        type: text
        text: 'Shrinkage on Knitwear and Sweatshirt products'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'When a garment is washed at a too high temperature or excessive heat is applied by either tumble drying or ironing it causes the garment fibres to melt which can either cause shrinkage or lose its elasticity. By washing the garment to the recommended 40 degrees and line drying, it will preserve the shape and durability of the garment.'
---
