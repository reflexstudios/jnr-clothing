---
id: e7511cd9-e733-4b34-8199-09eb23e20d3b
blueprint: pages
template: default
title: About
author: 45b2baf1-3b24-4a7e-9cfd-ee2c4a3aef1b
updated_by: 45b2baf1-3b24-4a7e-9cfd-ee2c4a3aef1b
updated_at: 1637072257
content:
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        text: 'Title goes here'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Text goes here'
---
