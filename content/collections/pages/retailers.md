---
id: 036dd262-1001-4878-9b2d-b89ac74b92e7
blueprint: pages
template: default
title: Retailers
author: 45b2baf1-3b24-4a7e-9cfd-ee2c4a3aef1b
updated_by: 36032470-b7c6-4fda-8fbc-d8a75800f3c4
updated_at: 1646710869
white_background: true
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Are you a retailer of school uniforms or workwear? We cater for retailers and schools as wholesale distributors.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.'
  -
    type: set
    attrs:
      values:
        type: get_a_quote_form
        get_a_quote_form_title: 'Get A Quote'
        get_a_quote_form_subtext: 'Let us know what you are looking for an we will come back to you as soon as possible with your custom quote.'
---
