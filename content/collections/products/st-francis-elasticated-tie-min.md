---
id: dc3d3a8e-6ecb-4b1c-9fec-f3758a0028f4
blueprint: products
gender: both
product_images:
  - ST-FRANCIS--ELASTICATED-TIE-min.jpg
title: 'ST FRANCIS  ELASTICATED TIE'
product_price: 3.99
has_variants: false
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638793502
---
