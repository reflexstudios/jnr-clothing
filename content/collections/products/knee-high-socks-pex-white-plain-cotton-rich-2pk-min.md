---
id: 5332fd74-82e7-44f3-b416-eb35e7829e68
blueprint: products
gender: girl
product_images:
  - KNEE-HIGH-SOCKS---PEX--WHITE-PLAIN-COTTON-RICH-2PK-min.jpg
title: 'KNEE HIGH SOCKS - PEX  WHITE PLAIN COTTON RICH 2PK-min'
has_variants: true
variants:
  -
    title: '6 - 8 ½'
    price: 5.99
  -
    title: '9 - 12'
    price: 5.99
  -
    title: '12 ½ - 3 ½'
    price: 5.99
  -
    title: '4 - 7'
    price: 6.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638373562
---
