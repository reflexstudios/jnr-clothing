---
id: 56bf0924-0317-4324-b71f-24f006d7f57b
blueprint: products
gender: girl
product_images:
  - BANNER-INVERT-PLEAT-PINAFORE.jpg
title: 'BANNER INVERT PLEAT PINAFORE'
has_variants: true
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1639132060
variants:
  -
    title: '3 - 4 years'
    price: '16.99'
  -
    title: '4 - 5 years'
    price: '16.99'
  -
    title: '5 - 6 years'
    price: '16.99'
  -
    title: '6 - 7 years'
    price: '16.99'
  -
    title: '7 - 8 years'
    price: '16.99'
  -
    title: '8 - 9 years'
    price: '16.99'
  -
    title: '9 - 10 years'
    price: '16.99'
  -
    title: '11 - 12 years'
    price: '16.99'
---
