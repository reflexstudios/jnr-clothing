---
id: 9d7c75ea-0e44-4b8d-bc5f-41ecd7d58089
blueprint: products
gender: both
product_images:
  - brownlow-min.jpg
title: 'brownlow tie-min'
product_price: 7.99
has_variants: false
updated_by: 3d9d4eff-dfbc-47e9-93db-fc3414303443
updated_at: 1639495377
---
