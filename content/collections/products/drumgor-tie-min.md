---
id: 6fec5917-be41-4ea4-bffb-f80220dad2e2
blueprint: products
gender: both
product_images:
  - DRUMGOR-TIE-min.jpg
title: 'DRUMGOR TIE'
product_price: 4.99
has_variants: false
variants:
  -
    title: null
    price: null
updated_by: 3d9d4eff-dfbc-47e9-93db-fc3414303443
updated_at: 1639495418
---
