---
id: c94bde92-2a59-4873-ae34-17c9946f541c
blueprint: products
gender: both
product_images:
  - NAVY-JOG-PANTS-min.jpg
title: 'NAVY JOG PANTS'
has_variants: true
variants:
  -
    title: '18'
    price: 10.99
  -
    title: '20'
    price: 10.99
  -
    title: '22'
    price: 10.99
  -
    title: '24'
    price: 10.99
  -
    title: '26'
    price: 10.99
  -
    title: '28'
    price: 10.99
  -
    title: '30'
    price: 10.99
  -
    title: '32'
    price: 10.99
  -
    title: '34'
    price: 11.99
  -
    title: '36'
    price: 11.99
  -
    title: '38'
    price: 12.99
  -
    title: '40'
    price: 12.99
  -
    title: '42'
    price: 13.99
  -
    title: '44'
    price: 13.99
  -
    title: '46'
    price: 15.99
  -
    title: '48'
    price: 15.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638787957
---
