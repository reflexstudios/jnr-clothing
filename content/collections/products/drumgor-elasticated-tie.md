---
id: 7327315e-171b-4d0a-a3e8-f69f177ddd10
blueprint: products
gender: both
product_images:
  - DRUMGOR--ELASTICATED-TIE-min.jpg
title: 'DRUMGOR  ELASTICATED TIE'
product_price: 3.99
has_variants: false
updated_by: 3d9d4eff-dfbc-47e9-93db-fc3414303443
updated_at: 1639495410
---
