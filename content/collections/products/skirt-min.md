---
id: 6c7b2087-461e-49ce-a5cc-54e2700da395
blueprint: products
gender: girl
product_images:
  - LISMORE-SKIRT-min-1637332815.jpg
title: SKIRT
has_variants: true
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1637332833
variants:
  -
    title: "24\twaist\t20 length"
    price: 29.99
  -
    title: "26\twaist\t20 length"
    price: 29.99
  -
    title: "28\twaist\t20 length"
    price: 29.99
  -
    title: "24\twaist\t22 length"
    price: 34.99
  -
    title: "26\twaist\t22 length"
    price: 29.99
  -
    title: "28\twaist\t22 length"
    price: 29.99
  -
    title: "30\twaist\t22 length"
    price: 34.99
  -
    title: "26\twaist\t24 length"
    price: 29.99
  -
    title: "28\twaist\t24 length"
    price: 29.99
  -
    title: "30\twaist\t24 length"
    price: 34.99
  -
    title: "32\twaist\t24 length"
    price: 34.99
  -
    title: "34\twaist\t24 length"
    price: 34.99
  -
    title: "36\twaist\t24 length"
    price: 34.99
  -
    title: "38\twaist\t24 length"
    price: 34.99
  -
    title: "40\twaist\t24 length"
    price: 34.99
  -
    title: "26\twaist\t26 length"
    price: 29.99
  -
    title: "28\twaist\t26 length"
    price: 29.99
  -
    title: "30\twaist\t26 length"
    price: 34.99
  -
    title: "32\twaist\t26 length"
    price: 34.99
  -
    title: "34\twaist\t26 length"
    price: 34.99
---
