---
id: 5b3dd412-e23a-4eea-a627-d062870397da
blueprint: products
gender: girl
product_images:
  - KNEE-HIGH-SOCKS---PEX-WHITE-PATTERN-COTTON-RICH--2-PK-min.jpg
title: 'KNEE HIGH SOCKS - PEX WHITE PATTERN COTTON RICH  2 PK-min'
has_variants: true
variants:
  -
    title: '6 - 8 ½'
    price: 5.99
  -
    title: '9 - 12'
    price: 5.99
  -
    title: '12 ½ - 3 ½'
    price: 5.99
  -
    title: '4 - 7'
    price: 6.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638373665
---
