---
id: a8987176-b096-4854-883f-1e1d641e0b2b
blueprint: products
gender: boy
product_images:
  - BOTTLE-GREEN-CREWNECK-SWEATSHIRT-min.jpg
title: 'BOTTLE GREEN CREWNECK SWEATSHIRT-min'
has_variants: true
variants:
  -
    title: '22'
    price: 12.99
  -
    title: '24'
    price: 12.99
  -
    title: '26'
    price: 12.99
  -
    title: '28'
    price: 12.99
  -
    title: '30'
    price: 12.99
  -
    title: '32'
    price: 12.99
  -
    title: '34'
    price: 13.99
  -
    title: '36'
    price: 13.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638375397
---
