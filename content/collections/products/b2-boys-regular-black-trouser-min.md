---
id: 26d8fe65-3c34-4dd2-91a2-943ef7eb90bf
blueprint: products
gender: boy
product_images:
  - B2.BOYS-REGULAR-BLACK-TROUSER-min.jpg
title: 'B2.BOYS REGULAR BLACK TROUSER'
has_variants: false
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1637054224
---
