---
id: f8991fa2-7e47-40d4-b487-a41362432bb8
blueprint: products
gender: girl
product_images:
  - TIGHTS---PEX-BLACK-COTTON-SOFT-min.jpg
title: 'TIGHTS - PEX BLACK COTTON SOFT-min'
has_variants: true
variants:
  -
    title: '3 - 5'
    price: 4.99
  -
    title: '5 -7'
    price: 4.99
  -
    title: '7 - 9'
    price: 4.99
  -
    title: '9 - 11'
    price: 4.99
  -
    title: '11 - 13'
    price: 4.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638371527
---
