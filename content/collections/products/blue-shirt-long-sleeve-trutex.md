---
id: a486a21f-aacc-4eba-91e9-5e97e223120e
blueprint: products
gender: boy
product_images:
  - BLUE-SHIRT-LONG-SLEEVE-TRUTEX.jpg
title: 'BLUE SHIRT LONG SLEEVE TRUTEX'
has_variants: true
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638370655
---
