---
id: 67c4f8bb-c9b1-496b-81fd-77da74658e68
blueprint: products
gender: both
product_images:
  - ST-CATHERINES-JUMPER-min.jpg
title: 'JUNIOR JUMPER'
has_variants: true
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1637330094
variants:
  -
    title: '32'
    price: 19.99
  -
    title: '34'
    price: 19.99
  -
    title: '36'
    price: 21.99
  -
    title: '38'
    price: 21.99
  -
    title: '40'
    price: 21.99
  -
    title: '42'
    price: 21.99
  -
    title: '44'
    price: 21.99
  -
    title: null
    price: null
---
