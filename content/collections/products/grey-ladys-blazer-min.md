---
id: eb37015c-9f37-4cfb-ba29-dcfaa6470821
blueprint: products
gender: girl
product_images:
  - LADYS-BLAZER-min.jpg
title: 'GREY LADYS BLAZER-min'
has_variants: true
variants:
  -
    title: '9'
    price: 55.99
  -
    title: '10'
    price: 55.99
  -
    title: '11'
    price: 55.99
  -
    title: '12'
    price: 55.99
  -
    title: '13'
    price: 55.99
  -
    title: '14'
    price: 55.99
  -
    title: '15'
    price: 55.99
  -
    title: '16'
    price: 65.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638364486
---
