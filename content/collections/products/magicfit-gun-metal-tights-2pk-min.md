---
id: 58ac3168-c614-4dc4-b279-166dd3624278
blueprint: products
gender: both
product_images:
  - MAGICFIT-GUN-METAL-TIGHTS-2PK-min.jpg
title: 'MAGICFIT GUN METAL TIGHTS 2PK-min'
has_variants: true
variants:
  -
    title: 'GUN METAL  TEENS  - MAGICFIT 2 PK'
    price: 6.99
  -
    title: 'GUN METAL  LADIES  - MAGICFIT 2 PK'
    price: 6.99
  -
    title: 'GUN METAL  XL - MAGICFIT  2 PK'
    price: 7.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638355684
---
