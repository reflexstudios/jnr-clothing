---
id: 423ae112-27bc-4acf-9c45-1b2e5a614203
blueprint: products
gender: girl
product_images:
  - WINE-KNITTED-CARDIGAN-min.jpg
title: 'WINE KNITTED CARDIGAN-min'
has_variants: true
variants:
  -
    title: '24'
    price: 14.99
  -
    title: '26'
    price: 15.99
  -
    title: '28'
    price: 15.99
  -
    title: '30'
    price: 16.99
  -
    title: '32'
    price: 16.99
  -
    title: '34'
    price: 17.99
  -
    title: '36'
    price: 17.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638377574
---
