---
id: 74ab7adc-b36e-498e-ab9b-5e5dcb8fb3f6
blueprint: products
gender: girl
product_images:
  - BLACK-KILT-PINAFORE.jpg
title: 'BLACK KILT PINAFORE'
has_variants: false
updated_by: 4bfa4be6-8be8-40f2-859e-bf8cbe8ba010
updated_at: 1648458308
product_price: 0
---
