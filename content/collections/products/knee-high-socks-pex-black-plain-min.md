---
id: 72e73126-5e72-4bd6-a23e-9b8174d679b5
blueprint: products
gender: girl
product_images:
  - KNEE-HIGH-SOCKS----PEX-BLACK-PLAIN-min.jpg
title: 'KNEE HIGH SOCKS  - PEX BLACK PLAIN-min'
has_variants: true
variants:
  -
    title: '6 – 8 ½'
    price: 5.99
  -
    title: '9 - 12'
    price: 5.99
  -
    title: '12 ½ - 3 ½'
    price: 5.99
  -
    title: '4 - 7'
    price: 6.99
  -
    title: '7 - 11'
    price: 7.50
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638366518
---
