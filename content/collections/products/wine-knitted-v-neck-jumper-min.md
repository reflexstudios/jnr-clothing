---
id: 76c34723-fab7-4fc1-b3c6-e0e4799e1e2a
blueprint: products
gender: both
product_images:
  - WINE-KNITTED--V-NECK-JUMPER-min-1638377411.jpg
title: 'WINE KNITTED  V NECK JUMPER-min'
has_variants: true
variants:
  -
    title: '24'
    price: 13.99
  -
    title: '26'
    price: 14.99
  -
    title: '28'
    price: 14.99
  -
    title: '30'
    price: 15.99
  -
    title: '32'
    price: 15.99
  -
    title: '34'
    price: 16.99
  -
    title: '36'
    price: 16.99
  -
    title: '38'
    price: 17.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638377503
---
