---
id: 078312a3-c735-4d76-8bc1-dd3deefe698d
blueprint: products
gender: boy
product_images:
  - NAVY-KNITTED-VNECK-JUMPER-min-1638447060.jpg
title: 'NAVY KNITTED VNECK JUMPER-min'
has_variants: true
variants:
  -
    title: '26'
    price: 14.99
  -
    title: '28'
    price: 14.99
  -
    title: '30'
    price: 15.99
  -
    title: '32'
    price: 15.99
  -
    title: '34'
    price: 16.99
  -
    title: '36'
    price: 16.99
  -
    title: '38'
    price: 17.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638447147
---
