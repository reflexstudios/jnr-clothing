---
id: ee6f5392-8879-4161-b821-f2ca6033a226
blueprint: products
gender: both
product_images:
  - TANNAGHMORE-PRIMARY-TIE-min.jpg
title: 'TANNAGHMORE PRIMARY TIE-min'
has_variants: true
variants:
  -
    title: 'Full Tie'
    price: 4.99
  -
    title: 'Elastic Tie'
    price: 3.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638374604
---
