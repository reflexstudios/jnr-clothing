---
id: 9895394f-7cd3-466d-8267-9f8ae1d403ab
blueprint: products
gender: boy
product_images:
  - ST-COLMANS-JUNIOR-TIE-min.jpg
title: 'ST COLMANS JUNIOR TIE-min'
product_price: 4.99
has_variants: false
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638363672
---
