---
id: 1c9b1d8a-3cdc-4f97-834d-e96b79d04368
blueprint: products
gender: both
product_images:
  - ROYAL-BLUE-CREWNECK-SWEATSHIRT-min-1638794671.jpg
title: 'ROYAL BLUE CREWNECK SWEATSHIRT'
has_variants: true
variants:
  -
    title: '3 - 4'
    price: 12.99
  -
    title: '5 - 6'
    price: 12.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638794712
---
