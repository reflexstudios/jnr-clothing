---
id: ca88971c-0e8e-47ad-9985-5f1f38e50805
blueprint: products
gender: both
product_images:
  - BLACK-JOG-BOTTOM-min-1638794457.jpg
title: 'BLACK JOG BOTTOM'
has_variants: true
variants:
  -
    title: '18'
    price: 10.99
  -
    title: '20'
    price: 10.99
  -
    title: '22'
    price: 10.99
  -
    title: '24'
    price: 10.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638794501
---
