---
id: 31451611-d7e7-49f1-9f11-9ae0a01e37a5
blueprint: products
gender: both
product_images:
  - WINE-SWEATSHIRT-min.jpg
title: 'WINE CREWNECK SWEATSHIRT'
has_variants: true
variants:
  -
    title: '22'
    price: 12.99
  -
    title: '24'
    price: 12.99
  -
    title: '26'
    price: 12.99
  -
    title: '28'
    price: 12.99
  -
    title: '30'
    price: 12.99
  -
    title: '32'
    price: 12.99
  -
    title: '34'
    price: 13.99
  -
    title: '36'
    price: 13.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638789671
---
