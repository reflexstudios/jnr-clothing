---
id: e6a8ae7e-0f37-406c-ada0-3396474d5b95
blueprint: products
gender: both
product_images:
  - ST-TERESAS--ELASTICATED-TIE-min.jpg
title: 'ST TERESAS  ELASTICATED TIE-min'
product_price: 3.99
has_variants: false
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638376983
---
