---
id: 13b5f7d0-4e16-4d74-af4a-60556696d69b
blueprint: products
gender: both
product_images:
  - ST-CATHERINES-GREEN-BLAZER-min.jpg
title: Blazer
has_variants: false
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1637053107
---
