---
id: 34fb12ec-c993-4fd4-94fb-2ef740808b4d
blueprint: products
gender: girl
product_images:
  - BANNER-ZIP-FRONT-PINAFORE.jpg
title: 'BANNER ZIP FRONT PINAFORE'
has_variants: true
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638792927
variants:
  -
    title: '3 - 4 years'
    price: 16.99
  -
    title: '4 - 5 years'
    price: 16.99
  -
    title: '5 - 6 years'
    price: 16.99
  -
    title: '6 - 7 years'
    price: 16.99
  -
    title: '7 - 8 years'
    price: 16.99
  -
    title: '8 - 9 years'
    price: 16.99
  -
    title: '9 - 10 years'
    price: 16.99
  -
    title: '11 - 12 years'
    price: 16.99
---
