---
id: 6416f9c8-5d78-4fcb-a218-244272412b09
blueprint: products
gender: girl
product_images:
  - SHORT-SOCKS---PEX-BOTTLE-GREEN-COTTON--2PK-min.jpg
title: 'SHORT SOCKS - PEX BOTTLE GREEN COTTON 2PK'
has_variants: true
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1637331515
variants:
  -
    title: '12 ½ - 3 ½'
    price: 5.99
  -
    title: '4 - 7'
    price: 5.99
  -
    title: 7-11
    price: 6.99
---
