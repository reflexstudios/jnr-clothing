---
id: 5473d2ac-2f4b-44f5-93bc-9a31abde351c
blueprint: products
gender: both
product_images:
  - WINE-CREWNECK-SWEATSHIRT-min.jpg
title: 'WINE CREWNECK SWEATSHIRT'
has_variants: false
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638789601
---
