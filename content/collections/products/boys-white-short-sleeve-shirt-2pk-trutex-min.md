---
id: a3cc85e8-da77-46d5-89c5-1c000ac34ac4
blueprint: products
gender: boy
product_images:
  - BOYS-WHITE-SHORT-SLEEVE-SHIRT-2PK-TRUTEX-min.jpg
title: 'BOYS WHITE SHORT SLEEVE SHIRT 2PK TRUTEX'
has_variants: false
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1637055130
---
