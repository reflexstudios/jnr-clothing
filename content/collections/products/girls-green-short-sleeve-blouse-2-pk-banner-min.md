---
id: 9a0d99ed-fc62-4bf6-9bc1-b2f398fe61b8
blueprint: products
gender: girl
product_images:
  - GIRLS-GREEN-SHORT-SLEEVE-BLOUSE-2-PK-BANNER-min.jpg
title: 'GIRLS GREEN SHORT SLEEVE BLOUSE 2 PK BANNER-min'
has_variants: true
variants:
  -
    title: '28'
    price: 16.99
  -
    title: '30'
    price: 16.99
  -
    title: '32'
    price: 16.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638375766
---
