---
id: 32c539d8-3ca1-409d-8509-2413a1c01c92
blueprint: products
gender: boy
product_images:
  - 20210713_172325-min.jpg
title: 'BOYS GREY BLAZER'
has_variants: true
variants:
  -
    title: '8'
    price: £58.99
  -
    title: '9'
    price: £58.99
  -
    title: '10'
    price: £58.99
  -
    title: '11'
    price: £58.99
  -
    title: '12'
    price: £58.99
  -
    title: '13'
    price: £62.99
  -
    title: '14'
    price: £62.99
  -
    title: '15'
    price: £62.99
  -
    title: '16'
    price: £64.99
  -
    title: '17'
    price: £64.99
  -
    title: '18'
    price: £64.99
  -
    title: '19'
    price: £64.99
  -
    title: '20'
    price: £64.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638363854
---
