---
id: eb99287d-dd8d-4fa6-bb09-1740ef3320df
blueprint: products
gender: both
product_images:
  - CEARA-TIE-min.jpg
title: 'CEARA TIE'
product_price: 4.99
has_variants: false
updated_by: 3d9d4eff-dfbc-47e9-93db-fc3414303443
updated_at: 1639495404
---
