---
id: d7b8df0c-7d95-4391-a7f0-7cc3fcfa25fb
blueprint: products
gender: boy
product_images:
  - JUNIOR-JUNMPER.jpg
title: 'JUNIOR JUNMPER'
has_variants: true
variants:
  -
    title: '32'
    price: 13.99
  -
    title: '34'
    price: 14.99
  -
    title: '36'
    price: 14.99
  -
    title: '38'
    price: 15.99
  -
    title: '40'
    price: 18.99
  -
    title: '42'
    price: 19.99
  -
    title: '44'
    price: 19.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638361783
---
