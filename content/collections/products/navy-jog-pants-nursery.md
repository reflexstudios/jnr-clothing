---
id: 80043099-3532-48e7-9216-b1a758a023d1
blueprint: products
gender: both
product_images:
  - NAVY-JOG-PANTS-min-1638794790.jpg
title: 'NAVY JOG PANTS'
has_variants: true
variants:
  -
    title: '18'
    price: 10.99
  -
    title: '20'
    price: 10.99
  -
    title: '22'
    price: 10.99
  -
    title: '24'
    price: 10.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638794824
---
