---
id: 2dd5fb41-683e-4892-bdd5-22d658383bc7
blueprint: products
gender: girl
product_images:
  - GIRLS-STRIPE-SHORT-SLEEVE-BLOUSE-2-PK-BANNER.jpg
title: 'GIRLS STRIPE SHORT SLEEVE BLOUSE 2 PK BANNER'
has_variants: true
variants:
  -
    title: '32'
    price: 21.99
  -
    title: '34'
    price: 21.99
  -
    title: '36'
    price: 21.99
  -
    title: '38'
    price: 27.99
  -
    title: '40'
    price: 27.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638354233
---
