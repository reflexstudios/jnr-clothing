---
id: 62e9555c-e55d-4491-9c3b-a9864d2612e8
blueprint: products
gender: boy
product_images:
  - BOYS-WHITE-LONG-SLEEVE-SHIRT-2PK-TRUTEX-min-1637065283.jpg
title: 'BOYS WHITE LONG SLEEVE SHIRT 2PK TRUTEX'
has_variants: false
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1637065974
---
