---
id: c16dc411-7542-4e72-b2f0-0f5e20490896
blueprint: products
gender: both
product_images:
  - ST-FRANCIS-TIE-min.jpg
title: 'ST FRANCIS TIE'
product_price: 4.99
has_variants: false
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638793476
---
