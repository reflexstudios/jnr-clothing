---
id: 82b8f99d-6c51-414e-8cd7-69b0593ff148
blueprint: products
gender: boy
product_images:
  - WHITES-TROUSER-WITH-ZIP-CLOSE-UP-min.jpg
  - WHITES-TROUSER-WITH-ZIP-min.jpg
title: 'WHITES NAVY TROUSER WITH ZIP-min'
has_variants: true
variants:
  -
    title: '6 - 7'
    price: 12.99
  -
    title: '7 - 8'
    price: 12.99
  -
    title: '8 - 9'
    price: 12.99
  -
    title: '9 - 10'
    price: 12.99
  -
    title: '10 - 11'
    price: 13.99
  -
    title: '11 - 12'
    price: 13.99
  -
    title: '12 - 13'
    price: 13.99
  -
    title: '13 - 14'
    price: 16.99
  -
    title: '14 - 15'
    price: 16.99
  -
    title: '15 - 16'
    price: 16.99
  -
    title: '16 - 17'
    price: 16.99
  -
    title: '17 - 18'
    price: 17.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638447419
---
