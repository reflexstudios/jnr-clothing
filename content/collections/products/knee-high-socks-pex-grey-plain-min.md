---
id: e61c553d-0ec3-4ff4-b64b-27bd8e864068
blueprint: products
gender: girl
product_images:
  - KNEE-HIGH-SOCKS----PEX-GREY-PLAIN-min.jpg
title: 'KNEE HIGH SOCKS  - PEX GREY PLAIN-min'
has_variants: true
variants:
  -
    title: '6 - 8 ½'
    price: 5.99
  -
    title: '9 - 12'
    price: 5.99
  -
    title: '12 ½ - 3 ½'
    price: 5.99
  -
    title: '4 - 7'
    price: 6.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638373495
---
