---
id: 63c7d6f6-3971-44ac-bac5-d0ad5d5c6d8f
blueprint: products
gender: boy
product_images:
  - B1.SENIOR-BOYS-SLIM-LEG-TROUSER-BLACK-min.jpg
title: 'SENIOR BOYS SLIM LEG TROUSER BLACK'
has_variants: false
variants:
  -
    title: null
    price: null
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1637331635
---
