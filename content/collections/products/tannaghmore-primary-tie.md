---
id: 7c08d31d-06d6-49b2-873a-a14778cdcf7c
blueprint: products
gender: both
product_images:
  - TANNAGHMORE-PRIMARY-TIE-min-1638375843.jpg
title: 'TANNAGHMORE PRIMARY TIE-min'
has_variants: true
variants:
  -
    title: 'Full Tie'
    price: 4.99
  -
    title: 'Elastic Tie'
    price: 3.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638375891
---
