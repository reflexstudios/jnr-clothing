---
id: 479265f5-cb91-4d28-b803-40c20da84d2b
blueprint: products
gender: girl
product_images:
  - SHORT-SOCKS--PEX-NAVY-2PK-min.jpg
title: 'SHORT SOCKS- PEX NAVY 2PK-min'
has_variants: true
variants:
  -
    title: '6 - 8 ½'
    price: 4.99
  -
    title: '9 - 12'
    price: 4.99
  -
    title: '12 ½ - 3 ½'
    price: 4.99
  -
    title: '4 - 7'
    price: 5.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638447232
---
