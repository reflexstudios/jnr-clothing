---
id: 51ff0161-eaba-4f8d-a359-c4e484192d99
blueprint: products
gender: both
product_images:
  - ST-JOHNS--ELASTICATED-TIE-min.jpg
title: 'ST JOHNS  ELASTICATED TIE'
product_price: 3.99
has_variants: false
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638794162
---
