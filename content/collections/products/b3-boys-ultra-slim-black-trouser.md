---
id: 5c7df86a-6808-46ee-96af-078d49b99f43
blueprint: products
gender: boy
product_images:
  - B3.BOYS-ULTRA-SLIM-BLACK-TROUSER.jpg
title: 'B3.BOYS ULTRA SLIM BLACK TROUSER'
has_variants: true
variants:
  -
    title: '26 waist 29 length'
    price: 21.99
  -
    title: '27 waist 29 length'
    price: 21.99
  -
    title: '28 waist 29 length'
    price: 21.99
  -
    title: '29 waist 29 length'
    price: 21.99
  -
    title: '30 waist 29 length'
    price: 21.99
  -
    title: '32 waist 29 length'
    price: 21.99
  -
    title: '34 waist 29 length'
    price: 21.99
  -
    title: '36 waist 29 length'
    price: 21.99
  -
    title: '26 waist 31 length'
    price: 21.99
  -
    title: '27 waist 31 length'
    price: 21.99
  -
    title: '28 waist 31 length'
    price: 21.99
  -
    title: '29 waist 31 length'
    price: 21.99
  -
    title: '30 waist 31 length'
    price: 21.99
  -
    title: '32 waist 31 length'
    price: 21.99
  -
    title: '34 waist 31 length'
    price: 21.99
  -
    title: '36 waist 31 length'
    price: 21.99
  -
    title: '26 waist 33 length'
    price: 21.99
  -
    title: '27 waist 33 length'
    price: 21.99
  -
    title: '28 waist 33 length'
    price: 21.99
  -
    title: '29 waist 33 length'
    price: 21.99
  -
    title: '30 waist 33 length'
    price: 21.99
  -
    title: '32 waist 33 length'
    price: 21.99
  -
    title: '34 waist 33 length'
    price: 21.99
  -
    title: '36 waist 33 length'
    price: 21.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638361231
---
