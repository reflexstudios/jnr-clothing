---
id: aa43912d-1a73-4a87-b11f-7bbe2db209be
blueprint: products
gender: both
product_images:
  - BLACK-V-NECK-SWEATSHIRT-min.jpg
title: 'BLACK V NECK SWEATSHIRT'
has_variants: true
variants:
  -
    title: '22'
    price: 12.99
  -
    title: '24'
    price: 12.99
  -
    title: '26'
    price: 12.99
  -
    title: '28'
    price: 12.99
  -
    title: '30'
    price: 12.99
  -
    title: '32'
    price: 12.99
  -
    title: '34'
    price: 13.99
  -
    title: '36'
    price: 13.99
  -
    title: '38'
    price: 14.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638790459
---
