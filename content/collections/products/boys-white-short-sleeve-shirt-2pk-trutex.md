---
id: c1cd4b51-333d-42fa-ab81-7220f1c71798
blueprint: products
gender: boy
product_images:
  - BOYS-WHITE-SHORT-SLEEVE-SHIRT-2PK-TRUTEX-min-1637065371.jpg
title: 'BOYS WHITE SHORT SLEEVE SHIRT 2PK TRUTEX'
has_variants: false
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1637065981
---
