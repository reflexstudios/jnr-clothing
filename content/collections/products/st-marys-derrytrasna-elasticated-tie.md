---
id: 486d5df8-2072-4218-a827-d7f3a1ac38a4
blueprint: products
gender: both
product_images:
  - ST-MARYS-DERRYTRASNA-ELASTICATED-TIE-min.jpg
title: 'ST MARYS DERRYTRASNA ELASTICATED TIE'
product_price: 3.99
has_variants: false
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638793875
---
