---
id: d00c5efd-22f9-4ad1-b624-e48f258297c0
blueprint: products
gender: boy
product_images:
  - BOYS-STRIPE-SHORT-SLEEVE-SHIRT-2-PK-BANNER.jpg
title: 'BOYS STRIPE SHORT SLEEVE SHIRT 2 PK BANNER'
has_variants: true
variants:
  -
    title: '13 ½'
    price: £19.99
  -
    title: '14'
    price: £19.99
  -
    title: '14 ½'
    price: £27.99
  -
    title: '15'
    price: £27.99
  -
    title: '15 ½'
    price: £27.99
  -
    title: '16'
    price: £27.99
  -
    title: '16 ½'
    price: £27.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638354113
---
