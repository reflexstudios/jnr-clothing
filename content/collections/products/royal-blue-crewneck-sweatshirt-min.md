---
id: 4a224712-d2d4-4af9-bc8d-4c59bd270d97
blueprint: products
gender: both
product_images:
  - ROYAL-BLUE-CREWNECK-SWEATSHIRT-min.jpg
title: 'ROYAL BLUE CREWNECK SWEATSHIRT'
has_variants: true
variants:
  -
    title: '22'
    price: 12.99
  -
    title: '24'
    price: 12.99
  -
    title: '26'
    price: 12.99
  -
    title: '28'
    price: 12.99
  -
    title: '30'
    price: 12.99
  -
    title: '32'
    price: 12.99
  -
    title: '34'
    price: 13.99
  -
    title: '36'
    price: 13.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638791512
---
