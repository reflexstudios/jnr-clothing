---
id: 426bd197-dc5b-4779-86b9-1cf78bf7cef8
blueprint: products
gender: both
product_images:
  - BLUE-CREWNECK-SWEATSHIRT-min.jpg
title: 'BLUE CREWNECK SWEATSHIRT-min'
has_variants: true
variants:
  -
    title: '22'
    price: 12.99
  -
    title: '24'
    price: 12.99
  -
    title: '26'
    price: 12.99
  -
    title: '28'
    price: 12.99
  -
    title: '30'
    price: 12.99
  -
    title: '32'
    price: 12.99
  -
    title: '34'
    price: 13.99
  -
    title: '36'
    price: 13.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638448046
---
