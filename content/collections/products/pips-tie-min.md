---
id: a3819dc7-665c-49a5-825d-d03acf849f2d
blueprint: products
gender: both
product_images:
  - PIPS-TIE-min.jpg
title: 'PIPS TIE-min'
product_price: 4.99
has_variants: false
updated_by: 3d9d4eff-dfbc-47e9-93db-fc3414303443
updated_at: 1639495460
---
