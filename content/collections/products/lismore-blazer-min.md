---
id: 1d767e3a-f741-4ec6-87c0-79a328833227
blueprint: products
gender: both
product_images:
  - LISMORE-BLAZER-min.jpg
title: BLAZER
has_variants: true
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1637332360
variants:
  -
    title: '6'
    price: '57.99'
  -
    title: '7'
    price: '57.99'
  -
    title: '8'
    price: '57.99'
  -
    title: '9'
    price: '57.99'
  -
    title: '10'
    price: '59.99'
  -
    title: '11'
    price: '59.99'
  -
    title: '12'
    price: '59.99'
  -
    title: '13'
    price: '59.99'
  -
    title: '14'
    price: '59.99'
  -
    title: '15'
    price: '59.99'
  -
    title: '16'
    price: '72.99'
  -
    title: '17'
    price: '72.99'
  -
    title: '18'
    price: '72.99'
  -
    title: '19'
    price: '72.99'
  -
    title: '21'
    price: '72.99'
  -
    title: '22'
    price: '72.99'
  -
    title: '23'
    price: '74.99'
  -
    title: '24'
    price: '74.99'
  -
    title: '25'
    price: '74.99'
  -
    title: '26'
    price: '74.99'
  -
    title: '27'
    price: '74.99'
  -
    title: '28'
    price: '74.99'
---
