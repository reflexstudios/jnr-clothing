---
id: fbb18614-1776-4ca8-924a-e0c9bd3bc65d
blueprint: products
gender: both
product_images:
  - LURGAN-MODEL-min.jpg
title: 'LURGAN MODEL tie-min'
product_price: 4.99
has_variants: false
updated_by: 3d9d4eff-dfbc-47e9-93db-fc3414303443
updated_at: 1639495442
---
