---
id: 9b479632-acd5-42f0-b102-5a851c1eea02
blueprint: products
gender: boy
product_images:
  - BLACK-WHITES-TROUSER-WITHOUT-ZIP-CLOSE-UP.jpg
  - BLACK-WHITES-TROUSER-WITHOUT-ZIP.jpg
title: 'BLACK WHITES TROUSER WITHOUT ZIP'
has_variants: true
variants:
  -
    title: '3 - 4'
    price: 10.99
  -
    title: '4 - 5'
    price: 10.99
  -
    title: '5 - 6'
    price: 10.99
  -
    title: '6 - 7'
    price: 10.99
  -
    title: '7 - 8'
    price: 10.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638372704
---
