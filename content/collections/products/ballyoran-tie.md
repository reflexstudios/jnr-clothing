---
id: 88d260c8-79eb-4879-8d8f-e1cd86481bed
blueprint: products
gender: both
product_images:
  - BALLYORAN-TIE-min.jpg
title: 'BALLYORAN TIE'
product_price: 4.99
has_variants: false
updated_by: 3d9d4eff-dfbc-47e9-93db-fc3414303443
updated_at: 1639495359
---
