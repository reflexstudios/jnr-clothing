---
id: ff016c01-061b-44a7-9e9f-e44f8cfa5ff1
blueprint: products
gender: girl
product_images:
  - TIGHTS---PEX-BOTTLE-COTTON-SOFT-min.jpg
title: 'TIGHTS - PEX BOTTLE COTTON SOFT-min'
has_variants: true
variants:
  -
    title: '3 - 5'
    price: 4.99
  -
    title: '5 - 7'
    price: 4.99
  -
    title: '7 - 9'
    price: 4.99
  -
    title: '9 - 11'
    price: 4.99
  -
    title: '11 - 13'
    price: 4.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638378512
---
