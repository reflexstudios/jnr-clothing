---
id: 8c6f7964-060f-4c27-8508-f9f545d60183
blueprint: products
gender: boy
product_images:
  - BLUE-SHIRT-SHORT-SLEEVE-TRUTEX-1638363298.jpg
title: 'BLUE SHIRT SHORT SLEEVE TRUTEX'
has_variants: false
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638363710
---
