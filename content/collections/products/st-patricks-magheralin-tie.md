---
id: 096dd99c-6db4-4532-8b64-d44def902ef3
blueprint: products
gender: both
product_images:
  - ST-PATRICKS-MAGHERALIN--ELASTICATED-TIE-min.jpg
title: 'ST PATRICKS MAGHERALIN ELASTICATED TIE-min'
product_price: 3.99
has_variants: false
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638379437
---
