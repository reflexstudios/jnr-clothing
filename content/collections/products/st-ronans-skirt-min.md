---
id: c271c9a7-c816-4378-b733-f90a9603171c
blueprint: products
gender: both
product_images:
  - ST-RONANS-SKIRT-min.jpg
title: 'ST RONANS SKIRT-min'
has_variants: true
variants:
  -
    title: '22 waist 20 length'
    price: 29.99
  -
    title: '24 waist 20 length'
    price: 29.99
  -
    title: '26 waist 20 length'
    price: 29.99
  -
    title: '24 waist 22 length'
    price: 29.99
  -
    title: '26 waist 22 length'
    price: 29.99
  -
    title: '28 waist 22 length'
    price: 29.99
  -
    title: '30 waist 22 length'
    price: 34.99
  -
    title: '32 waist 22 length'
    price: 34.99
  -
    title: '26 waist 24 length'
    price: 29.99
  -
    title: '28 waist 24 length'
    price: 29.99
  -
    title: '30 waist 24 length'
    price: 34.99
  -
    title: '32 waist 24 length'
    price: 34.99
  -
    title: '34 waist 24 length'
    price: 34.99
  -
    title: '36 waist 24 length'
    price: 34.99
  -
    title: '38 waist 24 length'
    price: 34.99
  -
    title: '40 waist 24 length'
    price: 34.99
  -
    title: '30 waist 26 length'
    price: 34.99
  -
    title: '32 waist 26 length'
    price: 34.99
  -
    title: '34 waist 26 length'
    price: 34.99
  -
    title: '36 waist 26 length'
    price: 34.99
  -
    title: '38 waist 26 length'
    price: 34.99
  -
    title: '40 waist 26 length'
    price: 34.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638359776
---
