---
id: aa4a8240-336f-4d8f-9d30-31e1db1d05ea
blueprint: products
gender: both
product_images:
  - BOTTLE-GREEN-KNITTED-VNECK-JUMPER-min.jpg
title: 'BOTTLE GREEN KNITTED VNECK JUMPER-min'
has_variants: true
variants:
  -
    title: '26'
    price: 14.99
  -
    title: '28'
    price: 14.99
  -
    title: '30'
    price: 15.99
  -
    title: '32'
    price: 15.99
  -
    title: '34'
    price: 16.99
  -
    title: '36'
    price: 16.99
  -
    title: '38'
    price: 17.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638375492
---
