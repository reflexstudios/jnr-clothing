---
id: ca60b12b-e6d3-46ea-993a-b95002faf227
blueprint: products
gender: girl
product_images:
  - brownlow-skirt-min.jpg
title: 'brownlow skirt-min'
has_variants: true
variants:
  -
    title: '24 waist 18 length'
    price: £21.99
  -
    title: '26 waist 18 length'
    price: £21.99
  -
    title: '28 waist 18 length'
    price: £21.99
  -
    title: '30 waist 18 length'
    price: £21.99
  -
    title: '32 waist 18 length'
    price: £21.99
  -
    title: '34 waist 18 length'
    price: £21.99
  -
    title: '36 waist 18 length'
    price: £21.99
  -
    title: '38 waist 18 length'
    price: £21.99
  -
    title: '40 waist 18 length'
    price: £21.99
  -
    title: '28 waist 20 length'
    price: £21.99
  -
    title: '30 waist 20 length'
    price: £21.99
  -
    title: '32 waist 20 length'
    price: £21.99
  -
    title: '34 waist 20 length'
    price: £21.99
  -
    title: '36 waist 20 length'
    price: £21.99
  -
    title: '38 waist 20 length'
    price: £21.99
  -
    title: '40 waist 20 length'
    price: £21.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638366328
---
