---
id: 534b6f39-47df-4a43-9ece-e1342934d378
blueprint: products
gender: boy
product_images:
  - GREY-WHITES-TROUSER-WITH-ZIP-CLOSE-UP.jpg
  - GREY-WHITES-TROUSER-WITH-ZIP.jpg
title: 'GREY WHITES TROUSER WITH ZIP'
has_variants: true
variants:
  -
    title: '6 - 7'
    price: 12.99
  -
    title: '7 - 8'
    price: 12.99
  -
    title: '8 - 9'
    price: 12.99
  -
    title: '9 - 10'
    price: 12.99
  -
    title: '10 - 11'
    price: 13.99
  -
    title: '11 - 12'
    price: 13.99
  -
    title: '12 - 13'
    price: 13.99
  -
    title: '13 - 14'
    price: 16.99
  -
    title: '14 - 15'
    price: 16.99
  -
    title: '15 - 16'
    price: 16.99
  -
    title: '16 - 17'
    price: 16.99
  -
    title: '17 - 18'
    price: 17.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638373073
---
