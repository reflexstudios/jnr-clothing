---
id: 5dd3f4d1-7ac6-432a-91aa-1998d3d17a71
blueprint: products
gender: girl
product_images:
  - GIRLS-GREEN-LONG-SLEEVE-BLOUSE-2-PK-BANNER-min.jpg
title: 'GIRLS GREEN LONG SLEEVE BLOUSE 2 PK BANNER-min'
has_variants: true
variants:
  -
    title: '28'
    price: 18.99
  -
    title: '30'
    price: 18.99
  -
    title: '32'
    price: 18.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638375697
---
