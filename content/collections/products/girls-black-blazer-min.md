---
id: cc94e903-b7fb-456f-95a8-7b1e0b494fa9
blueprint: products
gender: girl
product_images:
  - GIRLS-BLACK-BLAZER-min.jpg
title: 'GIRLS BLACK BLAZER-min'
has_variants: true
variants:
  -
    title: '6'
    price: 31.99
  -
    title: '7'
    price: 31.99
  -
    title: '8'
    price: 31.99
  -
    title: '9'
    price: 31.99
  -
    title: '10'
    price: 31.99
  -
    title: '11'
    price: 31.99
  -
    title: '12'
    price: 31.99
  -
    title: '13'
    price: 31.99
  -
    title: '14'
    price: 31.99
  -
    title: '15'
    price: 31.99
  -
    title: '16'
    price: 41.99
  -
    title: '17'
    price: 41.99
  -
    title: '18'
    price: 41.99
  -
    title: '19'
    price: 41.99
  -
    title: '20'
    price: 41.99
  -
    title: '21'
    price: 41.99
  -
    title: '22'
    price: 41.99
  -
    title: '23'
    price: 41.99
  -
    title: '24'
    price: 41.99
  -
    title: '25'
    price: 41.99
  -
    title: '26'
    price: 41.99
  -
    title: '27'
    price: 41.99
  -
    title: '28'
    price: 41.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638366155
---
