---
id: 603518ff-329d-4c11-ba12-cae279ae5a62
blueprint: products
gender: boy
product_images:
  - BLACK-WHITES-TROUSER-WITH-ZIP-CLOSE-UP.jpg
  - BLACK-WHITES-TROUSER-WITH-ZIP.jpg
title: 'BLACK WHITES TROUSER WITH ZIP'
has_variants: true
variants:
  -
    title: '3 - 4'
    price: 10.99
  -
    title: '4 - 5'
    price: 10.99
  -
    title: '5 - 6'
    price: 10.99
  -
    title: '6 - 7'
    price: 10.99
  -
    title: '7 - 8'
    price: 10.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638372608
---
