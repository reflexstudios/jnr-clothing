---
id: 8414a306-4656-41f2-b004-180effe7b23a
blueprint: products
gender: boy
product_images:
  - grey-trouser.jpg
title: 'ADULT SLIM FIT GREY TROUSER'
has_variants: true
variants:
  -
    title: '26 waist 29 length'
    price: 23.99
  -
    title: '27 waist 29 length'
    price: 23.99
  -
    title: '28 waist 29 length'
    price: 23.99
  -
    title: '29 waist 29 length'
    price: 23.99
  -
    title: '30 waist 29 length'
    price: 29.99
  -
    title: '32 waist 29 length'
    price: 29.99
  -
    title: '34 waist 29 length'
    price: 29.99
  -
    title: '36 waist 29 length'
    price: 29.99
  -
    title: '38 waist 29 length'
    price: 29.99
  -
    title: '26 waist 31 length'
    price: 23.99
  -
    title: '27 waist 31 length'
    price: 23.99
  -
    title: '28 waist 31 length'
    price: 23.99
  -
    title: '29 waist 31 length'
    price: 23.99
  -
    title: '30 waist 31 length'
    price: 29.99
  -
    title: '32 waist 31 length'
    price: 29.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638363688
---
