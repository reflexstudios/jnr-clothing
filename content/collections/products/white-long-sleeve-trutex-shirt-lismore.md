---
id: f0809e0b-4ac2-4733-a18c-080f107bc8f3
published: false
blueprint: products
gender: both
product_images:
  - WHITE-LONG-SLEEVE-TRUTEX-SHIRT-min-1637065834.jpg
title: 'WHITE LONG SLEEVE TRUTEX SHIRT'
has_variants: false
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1637332531
---
