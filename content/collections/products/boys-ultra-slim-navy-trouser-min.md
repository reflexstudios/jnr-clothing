---
id: 306104a1-13e7-4726-9754-63c2153ebf07
blueprint: products
gender: boy
product_images:
  - N1.BOYS-ULTRA-SLIM--NAVY-TROUSER-min.jpg
title: 'BOYS ULTRA SLIM  NAVY TROUSER'
has_variants: false
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1637332638
---
