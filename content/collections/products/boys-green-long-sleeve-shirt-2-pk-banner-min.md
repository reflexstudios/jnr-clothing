---
id: d5dd79e4-89ee-41af-be23-37df554e1935
blueprint: products
gender: boy
product_images:
  - BOYS-GREEN-LONG-SLEEVE-SHIRT-2-PK-BANNER-min.jpg
title: 'BOYS GREEN LONG SLEEVE SHIRT 2 PK BANNER-min'
has_variants: true
variants:
  -
    title: '12'
    price: £16.99
  -
    title: '12 ½'
    price: £16.99
  -
    title: '13'
    price: £16.99
  -
    title: '13 ½'
    price: £16.99
  -
    title: '14'
    price: £16.99
  -
    title: '14 ½'
    price: £24.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638375638
---
