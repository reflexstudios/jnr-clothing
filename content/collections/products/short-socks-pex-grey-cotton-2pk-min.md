---
id: fccfd3a6-9d15-4dee-a9e3-7549f3e34bb5
blueprint: products
gender: both
product_images:
  - SHORT-SOCKS---PEX-GREY--COTTON--2PK-min.jpg
title: 'SHORT SOCKS - PEX GREY  COTTON  2PK-min'
has_variants: true
variants:
  -
    title: '6 - 8 ½'
    price: 4.99
  -
    title: '9 - 12'
    price: 4.99
  -
    title: '12 ½ - 3 ½'
    price: 4.99
  -
    title: '4 - 7'
    price: 5.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638371370
---
