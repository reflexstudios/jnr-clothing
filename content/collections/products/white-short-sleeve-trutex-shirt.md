---
id: 1cbf63d9-5ebc-42c0-a3e0-739b203feac8
blueprint: products
gender: both
product_images:
  - WHITE-SHORT-SLEEVE-TRUTEX-SHIRT-min-1637065923.jpg
title: 'WHITE SHORT SLEEVE TRUTEX SHIRT'
has_variants: false
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1637065935
---
