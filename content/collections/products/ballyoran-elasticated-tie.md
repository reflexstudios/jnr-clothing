---
id: 0b6786b9-1976-4b40-99c0-34eb54f7b7bf
blueprint: products
gender: both
product_images:
  - BALLYORAN-ELASTICATED-TIE-min.jpg
title: 'BALLYORAN ELASTICATED TIE'
product_price: 3.99
has_variants: false
updated_by: 3d9d4eff-dfbc-47e9-93db-fc3414303443
updated_at: 1639495351
---
