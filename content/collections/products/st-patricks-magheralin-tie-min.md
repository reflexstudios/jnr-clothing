---
id: 3d8f77c0-ab90-4246-9e17-a8bdd0c5a96d
blueprint: products
gender: both
product_images:
  - ST-PATRICKS-MAGHERALIN-TIE-min-1638379352.jpg
title: 'ST PATRICKS MAGHERALIN TIE-min'
product_price: 4.99
has_variants: false
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638379366
---
