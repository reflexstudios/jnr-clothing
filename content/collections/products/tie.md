---
id: a9039b2a-ced9-4387-8e5b-32e6f3921d51
blueprint: products
gender: both
product_images:
  - 'st-Catherine''s-junior-min.jpg'
title: Tie
has_variants: true
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1637332727
variants:
  -
    title: 'Clip on tie'
    price: 7.99
---
