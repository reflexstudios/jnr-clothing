---
id: 806638ac-9974-41d1-bdf1-171d5f098c8c
blueprint: products
gender: both
product_images:
  - RED-POLO-SHIRT-min-1638794730.jpg
title: 'RED POLO SHIRT'
has_variants: true
variants:
  -
    title: '22'
    price: 4.99
  -
    title: '24'
    price: 4.99
  -
    title: '26'
    price: 4.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638794773
---
