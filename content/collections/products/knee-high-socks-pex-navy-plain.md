---
id: d077d2a2-e4ba-4c3a-8941-79c998176af5
blueprint: products
gender: girl
product_images:
  - KNEE-HIGH-SOCKS----PEX-NAVY--PLAIN-min-1638446562.jpg
title: 'KNEE HIGH SOCKS  - PEX NAVY  PLAIN-min'
has_variants: true
variants:
  -
    title: '6 - 8 ½'
    price: 5.99
  -
    title: '9 - 12'
    price: 5.99
  -
    title: '12 ½ - 3 ½'
    price: 5.99
  -
    title: '4 - 7'
    price: 6.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638446683
---
