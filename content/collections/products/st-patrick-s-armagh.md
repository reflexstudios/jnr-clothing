---
id: 4eb67065-1012-4595-a25a-f164d2ba80c4
blueprint: products
gender: boy
product_images:
  - 'st-Patrick''s-Armagh.jpg'
title: 'st Patrick''s Armagh'
product_price: 7.99
has_variants: false
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638362228
---
