---
id: 2fe2ecee-cf06-45a4-b96d-b2387633a9e2
blueprint: products
gender: girl
product_images:
  - BOTTLE-GREEN-KNITTED-CARDIGAN-min.jpg
title: 'BOTTLE GREEN KNITTED CARDIGAN-min'
has_variants: true
variants:
  -
    title: '24'
    price: 14.99
  -
    title: '26'
    price: 15.99
  -
    title: '28'
    price: 15.99
  -
    title: '30'
    price: 16.99
  -
    title: '32'
    price: 16.99
  -
    title: '34'
    price: 17.99
  -
    title: '36'
    price: 17.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638375295
---
