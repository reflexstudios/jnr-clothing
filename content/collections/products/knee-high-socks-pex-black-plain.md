---
id: 3bfcc30f-6465-49af-b454-aed168021368
blueprint: products
gender: girl
product_images:
  - KNEE-HIGH-SOCKS----PEX-BLACK-PLAIN.jpg
title: 'KNEE HIGH SOCKS  - PEX BLACK PLAIN'
has_variants: true
variants:
  -
    title: '6 - 8 ½'
    price: 5.99
  -
    title: '9 - 12'
    price: 5.99
  -
    title: '12 ½ - 3 ½'
    price: 5.99
  -
    title: '4 - 7'
    price: 6.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638373797
---
