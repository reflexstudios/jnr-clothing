---
id: 30b1317f-2010-471b-99e1-3c5e4a01fcbc
blueprint: products
gender: both
product_images:
  - BLUE-POLO-min.jpg
title: 'BLUE POLO-min'
has_variants: true
variants:
  -
    title: '22'
    price: 8.99
  -
    title: '24'
    price: 8.99
  -
    title: '26'
    price: 8.99
  -
    title: '28'
    price: 8.99
  -
    title: '30'
    price: 8.99
  -
    title: '32'
    price: 8.99
  -
    title: '34'
    price: 9.99
  -
    title: '36'
    price: 9.99
updated_by: 3d9d4eff-dfbc-47e9-93db-fc3414303443
updated_at: 1639491664
---
