---
id: 297fc9a4-fab9-4049-afb6-67c41f71a657
blueprint: products
gender: both
product_images:
  - BOTTLE-GREEN-BOOK-BAG-min.jpg
title: 'BOTTLE GREEN BOOK BAG-min'
product_price: 10.99
has_variants: false
updated_by: 3d9d4eff-dfbc-47e9-93db-fc3414303443
updated_at: 1639495368
---
