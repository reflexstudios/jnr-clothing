---
id: 3934db88-15f4-4c59-addd-f70b4c2b90e0
blueprint: products
gender: boy
product_images:
  - WHITES-TROUSER-WITHOUT-ZIP-min.jpg
title: 'WHITES NAVY TROUSER WITHOUT ZIP-min'
has_variants: true
variants:
  -
    title: '3 - 4'
    price: 10.99
  -
    title: '4 - 5'
    price: 10.99
  -
    title: '5 - 6'
    price: 10.99
  -
    title: '6 - 7'
    price: 10.99
  -
    title: '7 - 8'
    price: 10.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638447494
---
