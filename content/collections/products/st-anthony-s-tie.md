---
id: 26246cb0-4a32-445e-97c8-59e4581eb7d0
blueprint: products
gender: both
product_images:
  - 'st-Anthony''s-min.jpg'
title: 'st Anthony''s tie'
product_price: 7.99
has_variants: false
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638448502
---
