---
id: 1ef70579-c6ad-4a99-8f4a-a77d3c35ec90
blueprint: products
gender: boy
product_images:
  - BOYS-BLUE-LONG-SLEEVE-SHIRT-2PK-TRUTEX.jpg
title: 'BOYS BLUE LONG SLEEVE SHIRT 2PK TRUTEX'
has_variants: true
variants:
  -
    title: '12'
    price: £14.99
  -
    title: '12 ½'
    price: £14.99
  -
    title: '13'
    price: £14.99
  -
    title: '13 ½'
    price: £14.99
  -
    title: '14'
    price: £14.99
  -
    title: '14 ½'
    price: £18.99
  -
    title: '15'
    price: £18.99
  -
    title: '15 ½'
    price: £18.99
  -
    title: '16'
    price: £18.99
  -
    title: '16 ½'
    price: £18.99
  -
    title: '17'
    price: £20.99
  -
    title: '17 ½'
    price: £21.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638363718
---
