---
id: e0b81b1c-7fc0-4b3d-97a0-3af8203d2cf3
blueprint: products
gender: girl
product_images:
  - KNEE-HIGH-SOCKS----PEX-NAVY--PLAIN-min.jpg
title: 'KNEE HIGH SOCKS PEX PLAIN 2PK'
has_variants: true
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1637333501
variants:
  -
    title: '6 – 8 1/2'
    price: '5.99'
  -
    title: '9 - 12'
    price: '5.99'
  -
    title: '12 ½ - 3 ½'
    price: '5.99'
  -
    title: '4 - 7'
    price: '6.99'
  -
    title: 7-11
    price: '7.50'
---
