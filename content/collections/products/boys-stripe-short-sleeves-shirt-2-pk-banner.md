---
id: e7909e2c-dae0-4ed2-a5e4-1fedb81b1025
blueprint: products
gender: both
product_images:
  - BOYS-STRIPE-SHORT-SLEEVE-SHIRT-2-PK-BANNER-1638354862.jpg
title: 'BOYS STRIPE SHORT SLEEVE SHIRT 2 PK BANNER'
has_variants: true
variants:
  -
    title: '13 ½'
    price: £19.99
  -
    title: '14'
    price: £19.99
  -
    title: '14 ½'
    price: £27.99
  -
    title: '15'
    price: £27.99
  -
    title: '15 ½'
    price: £27.99
  -
    title: '16'
    price: £27.99
  -
    title: '16 ½'
    price: £27.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638355023
---
