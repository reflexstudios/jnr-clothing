---
id: 9cfa6ef6-5862-4077-9449-df15979317e2
blueprint: products
gender: both
product_images:
  - NAVY-CREWNECK-SWEATSHIRT-min.jpg
title: 'NAVY CREWNECK SWEATSHIRT'
has_variants: true
variants:
  -
    title: '22'
    price: 12.99
  -
    title: '24'
    price: 12.99
  -
    title: '26'
    price: 12.99
  -
    title: '28'
    price: 12.99
  -
    title: '30'
    price: 12.99
  -
    title: '32'
    price: 12.99
  -
    title: '34'
    price: 13.99
  -
    title: '36'
    price: 13.99
  -
    title: '38'
    price: 14.99
  -
    title: '40'
    price: 14.99
  -
    title: '42'
    price: 14.99
  -
    title: '44'
    price: 15.99
  -
    title: '46'
    price: 18.99
  -
    title: '48'
    price: 19.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638788194
---
