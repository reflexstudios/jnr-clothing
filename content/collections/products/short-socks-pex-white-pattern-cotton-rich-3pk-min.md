---
id: 521e7ecd-e5b1-4f99-a1cd-3d67091be618
blueprint: products
gender: both
product_images:
  - SHORT-SOCKS---PEX-WHITE-PATTERN-COTTON-RICH--3PK-min.jpg
title: 'SHORT SOCKS - PEX WHITE PATTERN COTTON RICH  3PK-min'
has_variants: true
variants:
  -
    title: '6 - 8 ½'
    price: 4.99
  -
    title: '9 - 12'
    price: 4.99
  -
    title: '12 ½ - 3 ½'
    price: 4.99
  -
    title: '4 - 7'
    price: 5.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638371447
---
