---
id: aae9733c-d28d-4d80-a6a5-4c37a4b5e906
blueprint: products
gender: both
product_images:
  - WHITE-SHORT-SLEEVE-TRUTEX-SHIRT-min.jpg
title: 'WHITE SHORT SLEEVE TRUTEX SHIRT'
has_variants: true
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1637329812
variants:
  -
    title: '12'
    price: 14.99
  -
    title: '12 ½'
    price: 14.99
  -
    title: '13'
    price: 14.99
  -
    title: '13 ½'
    price: 14.99
  -
    title: '14'
    price: 14.99
  -
    title: '14 ½'
    price: 18.99
  -
    title: '15'
    price: 18.99
  -
    title: '15 ½'
    price: 18.99
  -
    title: '16'
    price: 18.99
  -
    title: '16 ½'
    price: 18.99
  -
    title: '17'
    price: 18.99
  -
    title: '17 ½'
    price: 18.99
---
