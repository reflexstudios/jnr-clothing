---
id: 60ee6661-6977-41f5-9761-2eb2d3b85af1
blueprint: products
gender: both
product_images:
  - BLACK-JOG-BOTTOM-min.jpg
title: 'BLACK JOG BOTTOM'
has_variants: true
variants:
  -
    title: '18'
    price: 11.99
  -
    title: '20'
    price: 11.99
  -
    title: '22'
    price: 11.99
  -
    title: '24'
    price: 11.99
  -
    title: '26'
    price: 11.99
  -
    title: '28'
    price: 11.99
  -
    title: '30'
    price: 11.99
  -
    title: '32'
    price: 11.99
  -
    title: '34'
    price: 12.99
  -
    title: '36'
    price: 12.99
  -
    title: '38'
    price: 13.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638790384
---
