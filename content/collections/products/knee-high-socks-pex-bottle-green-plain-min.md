---
id: 06a9a0ec-bf90-4d87-88ea-63b434db9645
blueprint: products
gender: girl
product_images:
  - KNEE-HIGH-SOCKS----PEX-BOTTLE-GREEN-PLAIN-min.jpg
title: 'KNEE HIGH SOCKS  - PEX BOTTLE GREEN PLAIN'
has_variants: true
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1637331430
variants:
  -
    title: '12 ½ - 3 ½'
    price: '5.99'
  -
    title: '4 - 7'
    price: '6.99'
  -
    title: 7-11
    price: '7.50'
---
