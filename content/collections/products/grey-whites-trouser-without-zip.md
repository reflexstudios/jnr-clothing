---
id: 157b453c-0ec3-4b93-8bd6-54e9a7a4e382
blueprint: products
gender: boy
product_images:
  - GREY-WHITES-TROUSER-WITHOUT-ZIP-CLOSE-UP.jpg
  - GREY-WHITES-TROUSER-WITHOUT-ZIP.jpg
title: 'GREY WHITES TROUSER WITHOUT ZIP'
has_variants: true
variants:
  -
    title: '3 - 4'
    price: 10.99
  -
    title: '4 - 5'
    price: 10.99
  -
    title: '5 - 6'
    price: 10.99
  -
    title: '6 - 7'
    price: 10.99
  -
    title: '7 - 8'
    price: 10.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638373187
---
