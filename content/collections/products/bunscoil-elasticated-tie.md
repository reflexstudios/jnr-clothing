---
id: e08a086b-c988-4f01-af6a-567260d8822e
blueprint: products
gender: both
product_images:
  - BUNSCOIL--ELASTICATED-TIE-min.jpg
title: 'BUNSCOIL  ELASTICATED TIE'
product_price: 3.99
has_variants: false
updated_by: 3d9d4eff-dfbc-47e9-93db-fc3414303443
updated_at: 1639495383
---
