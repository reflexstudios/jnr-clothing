---
id: f40db013-a1a7-4301-ac97-e87554d6c4cb
blueprint: products
gender: both
product_images:
  - WINE-PE-SHORTS-min.jpg
title: 'WINE PE SHORTS-min'
has_variants: true
variants:
  -
    title: '18 - 20'
    price: 3.99
  -
    title: '22 - 24'
    price: 3.99
  -
    title: '26 - 28'
    price: 3.99
  -
    title: '30 - 32'
    price: 3.99
  -
    title: '34'
    price: 3.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638377629
---
