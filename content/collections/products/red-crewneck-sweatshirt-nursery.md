---
id: 0c587fa9-bfa8-4920-8701-4cb465b33ded
blueprint: products
gender: both
product_images:
  - RED-CREWNECK-SWEATSHIRT-min-1638794403.jpg
title: 'RED CREWNECK SWEATSHIRT'
has_variants: true
variants:
  -
    title: '22'
    price: 12.99
  -
    title: '24'
    price: 12.99
  -
    title: '26'
    price: 12.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638794446
---
