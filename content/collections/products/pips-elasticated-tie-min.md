---
id: 5ae4a55e-fde9-40ce-9bca-2224e169fd9a
blueprint: products
gender: both
product_images:
  - PORTADOWN-INTEGRATED--ELASTICATED-TIE-min.jpg
title: 'PIPS ELASTICATED TIE-min'
product_price: 3.99
has_variants: false
updated_by: 3d9d4eff-dfbc-47e9-93db-fc3414303443
updated_at: 1639495451
---
