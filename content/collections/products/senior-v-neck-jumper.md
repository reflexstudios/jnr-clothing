---
id: 669e6b1e-058a-4300-8a87-a38e9b8f9258
blueprint: products
gender: both
product_images:
  - NAVY-KNITTED-VNECK-JUMPER-min.jpg
title: 'SENIOR V NECK JUMPER'
has_variants: true
variants:
  -
    title: 'EXTRA SMALL'
    price: 20.99
  -
    title: SMALL
    price: 20.99
  -
    title: MEDIUM
    price: 25.99
  -
    title: LARGE
    price: 25.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1637330179
---
