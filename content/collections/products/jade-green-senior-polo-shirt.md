---
id: 7c2130fa-0523-4237-bd30-d8b3bc2f1935
blueprint: products
gender: both
product_images:
  - JADE-GREEN-SENIOR-POLO-SHIRT-min.jpg
title: 'JADE GREEN SENIOR POLO SHIRT'
has_variants: true
variants:
  -
    title: '7 - 8'
    price: 9.99
  -
    title: '9 - 10'
    price: 9.99
  -
    title: XXS
    price: 10.99
  -
    title: XS
    price: 10.99
  -
    title: S
    price: 10.99
  -
    title: M
    price: 10.99
  -
    title: L
    price: 10.99
  -
    title: XL
    price: 11.99
  -
    title: XXL
    price: 12.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638787808
---
