---
id: 38e5afee-76a9-4d50-853a-341ad0e94ec0
blueprint: products
gender: both
product_images:
  - LISMORE-SCARF-min.jpg
title: 'LISMORE SCARF'
has_variants: false
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1637065496
---
