---
id: be76e372-e013-4647-a0ef-87403efe2964
blueprint: products
gender: girl
product_images:
  - MAGICFIT-BOTTLE-TIGHTS-2-PK-min.jpg
title: 'MAGICFIT TIGHTS 2 PK'
has_variants: true
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1637333871
variants:
  -
    title: TEENS
    price: '6.99'
  -
    title: LADIES
    price: '6.99'
  -
    title: XL
    price: '7.99'
---
