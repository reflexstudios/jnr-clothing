---
id: d593d52e-27f5-466e-90ed-339170ceb56a
blueprint: products
gender: both
product_images:
  - BOTTLE-GREEN-KNITTED-VNECK-JUMPER-1638354654.jpg
title: 'BOTTLE GREEN KNITTED VNECK JUMPER'
has_variants: true
variants:
  -
    title: '30'
    price: £15.99
  -
    title: '32'
    price: £15.99
  -
    title: '34'
    price: £16.99
  -
    title: '36'
    price: £16.99
  -
    title: '38'
    price: £17.99
  -
    title: '40'
    price: £21.99
  -
    title: '42'
    price: £22.99
  -
    title: '44'
    price: £22.99
  -
    title: '46'
    price: £23.99
  -
    title: '48'
    price: £23.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638354802
---
