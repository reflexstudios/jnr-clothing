---
id: 499e604b-7d42-4637-86a8-b4da6beaceeb
blueprint: products
gender: girl
product_images:
  - GIRLS-YELLOW-LONG-SLEEVE--REVER-BLOUSE-2-PK-BANNER-min.jpg
title: 'GIRLS YELLOW LONG SLEEVE REVER BLOUSE 2 PK BANNER'
has_variants: true
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1637331989
variants:
  -
    title: '28'
    price: '15.99'
  -
    title: '30'
    price: '15.99'
  -
    title: '32'
    price: '15.99'
  -
    title: '34'
    price: '19.99'
  -
    title: '36'
    price: '19.99'
  -
    title: '38'
    price: '19.99'
  -
    title: '40'
    price: '19.99'
  -
    title: '42'
    price: '19.99'
  -
    title: '44'
    price: '19.99'
  -
    title: '46'
    price: '21.99'
---
