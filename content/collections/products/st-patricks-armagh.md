---
id: c0641526-e7ae-42d7-ab4c-a02943fe5762
blueprint: products
gender: boy
product_images:
  - 'st-Patrick''s-Armagh-1638362281.jpg'
title: 'st Patrick''s Armagh'
product_price: 7.99
has_variants: false
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638362305
---
