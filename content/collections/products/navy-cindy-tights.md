---
id: 6813cafd-f2c7-41e6-b917-26b37aa0780a
blueprint: products
gender: girl
product_images:
  - NAVY-CINDY-TIGHTS-min.jpg
title: 'NAVY CINDY TIGHTS'
has_variants: true
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1637333258
variants:
  -
    title: S
    price: '2.99'
  -
    title: M
    price: '2.99'
  -
    title: L
    price: '2.99'
---
