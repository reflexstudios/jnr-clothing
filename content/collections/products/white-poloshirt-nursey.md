---
id: 81e3bb2b-1213-4507-b0e2-589c8a140bf4
blueprint: products
gender: both
product_images:
  - WHITE-POLOSHIRT-min-1638794350.jpg
title: 'WHITE POLOSHIRT'
has_variants: true
variants:
  -
    title: '22'
    price: 8.99
  -
    title: '24'
    price: 8.99
  -
    title: '26'
    price: 8.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638794392
---
