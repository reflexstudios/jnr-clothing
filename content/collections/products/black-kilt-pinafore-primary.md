---
id: bba4b097-f85f-4b93-b486-63a0f60ee640
blueprint: products
gender: girl
product_images:
  - BLACK-KILT-PINAFORE-min.jpg
title: 'BLACK KILT PINAFORE'
has_variants: true
variants:
  -
    title: '2 - 3'
    price: 8.99
  -
    title: '3 - 4'
    price: 8.99
  -
    title: '4 - 5'
    price: 8.99
  -
    title: '5 - 6'
    price: 8.99
  -
    title: '6 - 7'
    price: 8.99
  -
    title: '7 - 8'
    price: 8.99
  -
    title: '8 - 9'
    price: 8.99
  -
    title: '9 - 10'
    price: 9.99
  -
    title: '11 - 12'
    price: 9.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638791003
---
