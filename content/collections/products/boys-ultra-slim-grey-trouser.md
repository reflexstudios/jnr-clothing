---
id: 7667db39-0ad2-4b3b-b1d1-c0909cef7815
blueprint: products
gender: boy
product_images:
  - G1.BOYS-ULTRA-SLIM-GREY-TROUSER-min.jpg
title: 'BOYS ULTRA SLIM GREY TROUSER'
has_variants: true
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1637330601
variants:
  -
    title: "26\t29 length"
    price: '21.99'
  -
    title: "27\t29 length"
    price: '21.99'
  -
    title: "28\t29 length"
    price: '21.99'
  -
    title: "29\t29 length"
    price: '21.99'
  -
    title: "30\t29 length"
    price: '21.99'
  -
    title: "32\t29 length"
    price: '21.99'
  -
    title: "34\t29 length"
    price: '21.99'
  -
    title: "36\t29 length"
    price: '21.99'
  -
    title: "26\t31 length"
    price: '21.99'
  -
    title: "27\t31 length"
    price: '21.99'
  -
    title: "28\t31 length"
    price: '21.99'
  -
    title: "29\t31 length"
    price: '21.99'
  -
    title: "30\t31 length"
    price: '21.99'
  -
    title: "32\t31 length"
    price: '21.99'
  -
    title: "34\t31 length"
    price: '21.99'
  -
    title: "36\t31 length"
    price: '21.99'
  -
    title: "26\t33 length"
    price: '21.99'
  -
    title: "27\t33 length"
    price: '21.99'
  -
    title: "28\t33 length"
    price: '21.99'
  -
    title: "29\t33 length"
    price: '21.99'
  -
    title: "30\t33 length"
    price: '21.99'
  -
    title: "32\t33 length"
    price: '21.99'
  -
    title: "34\t33 length"
    price: '21.99'
  -
    title: "36\t33 length"
    price: '21.99'
---
