---
id: be05d080-42a1-464c-a3bd-2b87d7ac8fcb
blueprint: products
gender: both
product_images:
  - NAVY-KNITTED-VNECK-JUMPER-min.jpg
title: 'NAVY KNITTED VNECK JUMPER'
has_variants: true
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1637332512
variants:
  -
    title: '30'
    price: 19.99
  -
    title: '32'
    price: 19.99
  -
    title: '34'
    price: 19.99
  -
    title: '36'
    price: 19.99
  -
    title: '38'
    price: 19.99
  -
    title: '40'
    price: 26.99
  -
    title: '42'
    price: 26.99
  -
    title: '44'
    price: 26.99
  -
    title: '46'
    price: 26.99
---
