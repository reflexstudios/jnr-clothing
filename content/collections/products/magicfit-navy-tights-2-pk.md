---
id: f953dda1-d945-4f32-af9b-fcfb8fde3d58
blueprint: products
gender: girl
product_images:
  - MAGICFIT-NAVY-TIGHTS-2-PK-min.jpg
title: 'MAGICFIT NAVY TIGHTS 2 PK'
has_variants: true
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1637333220
variants:
  -
    title: TEENS
    price: '6.99'
  -
    title: LADIES
    price: '6.99'
  -
    title: XL
    price: '7.99'
---
