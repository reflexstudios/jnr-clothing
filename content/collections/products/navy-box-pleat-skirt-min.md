---
id: 8229bb52-1c9b-4a95-bc49-153d5e2c8956
blueprint: products
gender: girl
product_images:
  - NAVY-BOX-PLEAT-SKIRT-min.jpg
title: 'NAVY BOX PLEAT SKIRT-min'
has_variants: true
variants:
  -
    title: '2 - 3'
    price: null
  -
    title: '3 - 4'
    price: null
  -
    title: '4 - 5'
    price: null
  -
    title: '5 - 6'
    price: null
  -
    title: '6 - 7'
    price: null
  -
    title: '7 - 8'
    price: null
  -
    title: '8 - 9'
    price: null
  -
    title: '9 - 10'
    price: null
  -
    title: '11- 12'
    price: null
  -
    title: '13 - 14'
    price: null
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638446815
---
