---
id: 9a9eb1fc-f213-49f0-8dd5-04d1cc57cea2
blueprint: products
gender: both
product_images:
  - RED-POLO-SHIRT-min.jpg
title: 'RED POLO SHIRT'
has_variants: true
variants:
  -
    title: '22'
    price: 8.99
  -
    title: '24'
    price: 8.99
  -
    title: '26'
    price: 8.99
  -
    title: '28'
    price: 8.99
  -
    title: '30'
    price: 8.99
  -
    title: '32'
    price: 8.99
  -
    title: '34'
    price: 9.99
  -
    title: '36'
    price: 9.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638788030
---
