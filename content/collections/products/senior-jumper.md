---
id: e90d9f5d-72ee-4f4c-85b3-482dcb164618
blueprint: products
gender: boy
product_images:
  - SENIOR--JUMPER.jpg
title: 'SENIOR  JUMPER'
has_variants: true
variants:
  -
    title: '38'
    price: 15.99
  -
    title: '40'
    price: 18.99
  -
    title: '42'
    price: 19.99
  -
    title: '44'
    price: 19.99
  -
    title: '46'
    price: 20.99
  -
    title: '48'
    price: 20.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638362007
---
