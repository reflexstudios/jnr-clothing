---
id: 02632c57-949d-45c8-8e72-c08e28438cf6
blueprint: products
gender: girl
product_images:
  - NAVY-KILT-PINAFORE-min.jpg
title: 'NAVY KILT PINAFORE-min'
has_variants: true
variants:
  -
    title: '2 - 3'
    price: null
  -
    title: '3 - 4'
    price: null
  -
    title: '4 - 5'
    price: null
  -
    title: '5 - 6'
    price: null
  -
    title: '6 - 7'
    price: null
  -
    title: '7 - 8'
    price: null
  -
    title: '8 - 9'
    price: null
  -
    title: '9 - 10'
    price: null
  -
    title: '11 - 12'
    price: null
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638446975
---
