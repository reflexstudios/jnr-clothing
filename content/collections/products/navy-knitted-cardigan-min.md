---
id: bbade44c-0ab3-495e-8154-da9cce390b84
blueprint: products
gender: girl
product_images:
  - NAVY-KNITTED-CARDIGAN-min.jpg
title: 'NAVY KNITTED CARDIGAN-min'
has_variants: true
variants:
  -
    title: '24'
    price: 14.99
  -
    title: '26'
    price: 15.99
  -
    title: '28'
    price: 15.99
  -
    title: '30'
    price: 16.99
  -
    title: '32'
    price: 16.99
  -
    title: '34'
    price: 17.99
  -
    title: '36'
    price: 17.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638447042
---
