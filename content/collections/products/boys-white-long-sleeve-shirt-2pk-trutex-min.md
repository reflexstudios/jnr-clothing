---
id: dc34bf7f-2a4b-4779-90e5-ce4b72ce0f33
blueprint: products
gender: boy
product_images:
  - BOYS-WHITE-LONG-SLEEVE-SHIRT-2PK-TRUTEX-min.jpg
title: 'WHITE LONG SLEEVE SHIRT 2PK TRUTEX'
has_variants: false
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1637332592
---
