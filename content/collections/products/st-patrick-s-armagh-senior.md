---
id: f27745ee-433e-49da-a7a1-138a723be405
blueprint: products
gender: boy
product_images:
  - 'st-Patrick''s-armagh-senior.jpg'
title: 'st Patrick''s armagh senior'
product_price: 7.99
has_variants: false
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638362234
---
