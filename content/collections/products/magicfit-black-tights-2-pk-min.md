---
id: 779bfd32-afcf-4fd4-9cee-7c9ccb9fd44d
blueprint: products
gender: girl
product_images:
  - MAGICFIT-BLACK-TIGHTS-2-PK-min.jpg
title: 'MAGICFIT BLACK TIGHTS 2 PK-min'
has_variants: true
variants:
  -
    title: S
    price: 2.99
  -
    title: M
    price: 2.99
  -
    title: L
    price: 2.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638366592
---
