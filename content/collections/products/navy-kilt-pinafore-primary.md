---
id: c203b15e-a39f-4cc9-866a-e826675a226d
blueprint: products
gender: girl
product_images:
  - NAVY-KILT-PINAFORE-min-1638789827.jpg
title: 'NAVY KILT PINAFORE-min'
has_variants: true
variants:
  -
    title: '2 - 3'
    price: 8.99
  -
    title: '3 - 4'
    price: 8.99
  -
    title: '4 - 5'
    price: 8.99
  -
    title: '5 - 6'
    price: 8.99
  -
    title: '6 - 7'
    price: 8.99
  -
    title: '7 - 8'
    price: 8.99
  -
    title: '8 - 9'
    price: 8.99
  -
    title: '9 - 10'
    price: 9.99
  -
    title: '11 - 12'
    price: 9.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638789933
---
