---
id: c11c0747-6a92-4a30-a7e8-381be84e4442
blueprint: products
gender: girl
product_images:
  - BLACK-BOX-PLEAT-SKIRT.jpg
title: 'BLACK BOX PLEAT SKIRT'
has_variants: true
variants:
  -
    title: '2 - 3'
    price: 5.99
  -
    title: '3 - 4'
    price: 5.99
  -
    title: '4 - 5'
    price: 5.99
  -
    title: '5 - 6'
    price: 5.99
  -
    title: '6 - 7'
    price: 5.99
  -
    title: '7 - 8'
    price: 5.99
  -
    title: '8 -9'
    price: 5.99
  -
    title: '9 - 10'
    price: 5.99
  -
    title: '11 - 12'
    price: 5.99
  -
    title: '13 - 14'
    price: 6.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638372406
---
