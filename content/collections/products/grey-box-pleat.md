---
id: cf3f7e19-1eb6-4800-b0ad-827db191a1a7
blueprint: products
gender: girl
product_images:
  - GREY-BOX-PLEAT.jpg
title: 'GREY BOX PLEAT'
has_variants: true
variants:
  -
    title: '2 - 3'
    price: 5.99
  -
    title: '3 - 4'
    price: 5.99
  -
    title: '4 - 5'
    price: 5.99
  -
    title: '5 - 6'
    price: 5.99
  -
    title: '6 - 7'
    price: 5.99
  -
    title: '7 - 8'
    price: 5.99
  -
    title: '8 - 9'
    price: 5.99
  -
    title: '9 - 10'
    price: 5.99
  -
    title: '11 - 12'
    price: 5.99
  -
    title: '13 - 14'
    price: 6.99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638793156
---
