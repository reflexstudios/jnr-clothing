---
id: d63159b0-f151-421b-9814-dc9952494b00
blueprint: products
gender: girl
product_images:
  - GIRLS-YELLOW-LONG-SLEEVE-BLOUSE-2-PK-BANNER-min.jpg
title: 'GIRLS YELLOW LONG SLEEVE BLOUSE 2 PK BANNER'
has_variants: false
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1637054434
---
