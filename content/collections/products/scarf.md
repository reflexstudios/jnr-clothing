---
id: 5bf497a7-0b8d-401d-bb60-d1ec83a601a3
blueprint: products
gender: both
product_images:
  - SCARF-min.jpg
title: SCARF
has_variants: true
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1637330952
variants:
  -
    title: STRIPE
    price: 17.99
---
