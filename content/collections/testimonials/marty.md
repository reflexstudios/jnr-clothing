---
id: bee51503-5078-4084-b155-53d8c330c1a8
blueprint: testimonials
stars: four
title: 'Sharon Doran - Tommy French Bookmakers'
review: 'We get all our uniforms from J&R clothing.  The service is very efficient and Babita is very good to deal with.  The quality of the blouses is very good as well, no fading when washed.  When I requested the logo to be changed nothing was a problem.'
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1637320644
---
