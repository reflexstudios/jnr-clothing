---
id: c21861ec-78bf-4652-aaac-70edaa4a3096
blueprint: testimonials
stars: five
title: Brenda
review: |-
  I have shopped at JNR Clothing for a number of years now and have always found their clothing and Uniforms to be great quality.  They offer a great service.  With extended opening hours to suit everyone during busy periods.  I have always been treated with a smile and great professionalism when it comes to helping me choosing the correct items and sizes.  
  I am happy to recommend JNR Clothing.
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1637320380
---
