---
id: ad9db0f1-4d42-4520-86fe-948125331fce
blueprint: testimonials
title: 'Feargal McGrann - Campbells Pharmacy'
testimonial:
  -
    stars: five
    reviewer: Jimmy
    review: 'They''re class so they are'
  -
    stars: four
    reviewer: Marty
    review: 'I''m not longer naked with this great workwear.'
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1637320616
stars: five
review: |-
  The pharmacy has been using J+R Clothing as sole provider of our uniform tunics, polo shirts,
  and fleeces for several years now. The staff always find these to be of good quality, comfortable
  fit and say they wash well. Uniforms provided by J+R Clothing were worn in our Lurgan store
  initially and the owner was so impressed with the quality that she introduced the uniforms into our
  Belfast branches also. A selection of colours and the option of embroidery is also a bonus for
  individual store preferences.
---
