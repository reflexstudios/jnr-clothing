---
id: 981ad808-f9b9-43a9-9f7b-62a51eb768f1
blueprint: schools
school_logo: carrick-min.JPG
school_type: primary
title: 'CARRICK PS'
products:
  - 56bf0924-0317-4324-b71f-24f006d7f57b
  - 34fb12ec-c993-4fd4-94fb-2ef740808b4d
  - 4fff0036-4d2a-4628-84e5-e7cbea6be9b0
  - 30b1317f-2010-471b-99e1-3c5e4a01fcbc
  - 1b0ca9ce-4306-47e6-8431-d18b48ae93c5
  - 8c6f7964-060f-4c27-8508-f9f545d60183
  - cf3f7e19-1eb6-4800-b0ad-827db191a1a7
  - 51c3d210-e82d-44b9-b32e-ca6e1f381232
  - 534b6f39-47df-4a43-9ece-e1342934d378
  - 157b453c-0ec3-4b93-8bd6-54e9a7a4e382
  - e9b09ca9-7812-44ce-b1ad-e0a15ee7d493
  - 60ee6661-6977-41f5-9761-2eb2d3b85af1
  - aa43912d-1a73-4a87-b11f-7bbe2db209be
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638790483
---
