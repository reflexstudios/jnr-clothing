---
id: 9869da27-2e65-4afd-9272-b7495c03a9cf
blueprint: schools
school_logo: st-anthonys-min.JPG
school_type: primary
title: 'ST ANTHONY''S CRAIGAVON'
products:
  - 56bf0924-0317-4324-b71f-24f006d7f57b
  - 34fb12ec-c993-4fd4-94fb-2ef740808b4d
  - a486a21f-aacc-4eba-91e9-5e97e223120e
  - 1b0ca9ce-4306-47e6-8431-d18b48ae93c5
  - 5332fd74-82e7-44f3-b416-eb35e7829e68
  - 5b3dd412-e23a-4eea-a627-d062870397da
  - 521e7ecd-e5b1-4f99-a1cd-3d67091be618
  - 4fff0036-4d2a-4628-84e5-e7cbea6be9b0
  - 426bd197-dc5b-4779-86b9-1cf78bf7cef8
  - 30b1317f-2010-471b-99e1-3c5e4a01fcbc
  - cf3f7e19-1eb6-4800-b0ad-827db191a1a7
  - 51c3d210-e82d-44b9-b32e-ca6e1f381232
  - 60b31668-8f57-48b9-8852-44130da97442
  - 534b6f39-47df-4a43-9ece-e1342934d378
  - 157b453c-0ec3-4b93-8bd6-54e9a7a4e382
  - fccfd3a6-9d15-4dee-a9e3-7549f3e34bb5
  - e9b09ca9-7812-44ce-b1ad-e0a15ee7d493
  - ed86c7fa-422b-4975-90ae-67537ea9de51
  - 9d2a9354-312b-4331-bd92-235cfe61394e
  - 26246cb0-4a32-445e-97c8-59e4581eb7d0
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638448626
---
