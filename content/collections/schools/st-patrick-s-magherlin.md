---
id: d793fec1-e223-496f-bf3c-5325d2337462
blueprint: schools
school_logo: st-patricks-magherlin-min.JPG
school_type: primary
title: 'ST PATRICK''S MAGHERLIN'
products:
  - 56bf0924-0317-4324-b71f-24f006d7f57b
  - 34fb12ec-c993-4fd4-94fb-2ef740808b4d
  - 188bf702-1471-4e5c-9c53-8c8a98bddb9c
  - 8fc9bc55-41cc-40f0-bef5-8c4045ebabd1
  - 2fe2ecee-cf06-45a4-b96d-b2387633a9e2
  - aa4a8240-336f-4d8f-9d30-31e1db1d05ea
  - cf3f7e19-1eb6-4800-b0ad-827db191a1a7
  - 51c3d210-e82d-44b9-b32e-ca6e1f381232
  - 534b6f39-47df-4a43-9ece-e1342934d378
  - 157b453c-0ec3-4b93-8bd6-54e9a7a4e382
  - 06a9a0ec-bf90-4d87-88ea-63b434db9645
  - 72e73126-5e72-4bd6-a23e-9b8174d679b5
  - 5332fd74-82e7-44f3-b416-eb35e7829e68
  - 12b09bb6-290a-496b-80fe-925b06a44ef9
  - 5b3dd412-e23a-4eea-a627-d062870397da
  - 6416f9c8-5d78-4fcb-a218-244272412b09
  - fccfd3a6-9d15-4dee-a9e3-7549f3e34bb5
  - 521e7ecd-e5b1-4f99-a1cd-3d67091be618
  - e9b09ca9-7812-44ce-b1ad-e0a15ee7d493
  - 3d8f77c0-ab90-4246-9e17-a8bdd0c5a96d
  - 096dd99c-6db4-4532-8b64-d44def902ef3
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638379442
---
