---
id: 05dc9194-91d3-4777-bec2-05107699ab94
blueprint: schools
school_logo: lismore.JPG
school_type: secondary
title: 'Lismore Comprehensive School'
products:
  - 62e9555c-e55d-4491-9c3b-a9864d2612e8
  - c1cd4b51-333d-42fa-ab81-7220f1c71798
  - 1d767e3a-f741-4ec6-87c0-79a328833227
  - 38e5afee-76a9-4d50-853a-341ad0e94ec0
  - f953dda1-d945-4f32-af9b-fcfb8fde3d58
  - 6813cafd-f2c7-41e6-b917-26b37aa0780a
  - be05d080-42a1-464c-a3bd-2b87d7ac8fcb
  - 1cbf63d9-5ebc-42c0-a3e0-739b203feac8
  - dc34bf7f-2a4b-4779-90e5-ce4b72ce0f33
  - a3cc85e8-da77-46d5-89c5-1c000ac34ac4
  - a9039b2a-ced9-4387-8e5b-32e6f3921d51
  - 6c7b2087-461e-49ce-a5cc-54e2700da395
  - 7667db39-0ad2-4b3b-b1d1-c0909cef7815
  - 63c7d6f6-3971-44ac-bac5-d0ad5d5c6d8f
  - e0b81b1c-7fc0-4b3d-97a0-3af8203d2cf3
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1637333510
---
