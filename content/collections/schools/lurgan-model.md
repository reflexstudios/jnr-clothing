---
id: 49552579-94f2-42b2-b416-620685e78b84
blueprint: schools
school_logo: lurgan-model-crest-min.JPG
school_type: primary
title: 'LURGAN MODEL'
products:
  - fccfd3a6-9d15-4dee-a9e3-7549f3e34bb5
  - ff016c01-061b-44a7-9e9f-e44f8cfa5ff1
  - 60b31668-8f57-48b9-8852-44130da97442
  - 6fb25985-d718-469f-b701-ba4a04b01740
  - aae9733c-d28d-4d80-a6a5-4c37a4b5e906
  - e9b09ca9-7812-44ce-b1ad-e0a15ee7d493
  - c8fb9211-a185-482d-a7fb-dd39cdef27d5
  - fbb18614-1776-4ca8-924a-e0c9bd3bc65d
  - 56bf0924-0317-4324-b71f-24f006d7f57b
  - 34fb12ec-c993-4fd4-94fb-2ef740808b4d
  - 51c3d210-e82d-44b9-b32e-ca6e1f381232
  - cf3f7e19-1eb6-4800-b0ad-827db191a1a7
  - 534b6f39-47df-4a43-9ece-e1342934d378
  - 157b453c-0ec3-4b93-8bd6-54e9a7a4e382
  - e61c553d-0ec3-4ff4-b64b-27bd8e864068
  - 5b3dd412-e23a-4eea-a627-d062870397da
  - 5332fd74-82e7-44f3-b416-eb35e7829e68
  - 0a5a427f-f1f5-4bf3-a7d1-23587c5cc376
  - 2fe2ecee-cf06-45a4-b96d-b2387633a9e2
  - aa4a8240-336f-4d8f-9d30-31e1db1d05ea
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638449976
---
