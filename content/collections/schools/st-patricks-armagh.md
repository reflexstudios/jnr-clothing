---
id: 666ec75a-fc91-4f43-a3f7-6f63379b8875
blueprint: schools
school_logo: st-patricks.jpg
school_type: secondary
title: 'ST PATRICKS ARMAGH'
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638362368
products:
  - 26d8fe65-3c34-4dd2-91a2-943ef7eb90bf
  - 5c7df86a-6808-46ee-96af-078d49b99f43
  - 63c7d6f6-3971-44ac-bac5-d0ad5d5c6d8f
  - 1cbf63d9-5ebc-42c0-a3e0-739b203feac8
  - 6fb25985-d718-469f-b701-ba4a04b01740
  - 62e9555c-e55d-4491-9c3b-a9864d2612e8
  - c1cd4b51-333d-42fa-ab81-7220f1c71798
  - bcc8f2a6-2e0f-4f01-bed5-15bd2738094e
  - d7b8df0c-7d95-4391-a7f0-7cc3fcfa25fb
  - e90d9f5d-72ee-4f4c-85b3-482dcb164618
  - f27745ee-433e-49da-a7a1-138a723be405
  - c0641526-e7ae-42d7-ab4c-a02943fe5762
---
