---
id: 6387d0af-a988-4587-bf98-62778f307474
blueprint: schools
school_logo: our-ladys-newry-min.JPG
school_type: secondary
title: 'OUR LADY''S NEWRY'
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638365643
products:
  - c1cd4b51-333d-42fa-ab81-7220f1c71798
  - 62e9555c-e55d-4491-9c3b-a9864d2612e8
  - eb37015c-9f37-4cfb-ba29-dcfaa6470821
  - d13a52a1-cc52-4cb8-8085-7336e1f2cb3d
  - 900f4811-cab4-4383-af0a-3e7e19824624
---
