---
id: 3264eb5d-44f0-4ddc-b543-379d565d6d4e
blueprint: schools
school_logo: aghagallon-min.JPG
school_type: primary
title: 'ST PATRICK''S AGHAGALLON'
products:
  - 56bf0924-0317-4324-b71f-24f006d7f57b
  - 34fb12ec-c993-4fd4-94fb-2ef740808b4d
  - 30b1317f-2010-471b-99e1-3c5e4a01fcbc
  - 5332fd74-82e7-44f3-b416-eb35e7829e68
  - 5b3dd412-e23a-4eea-a627-d062870397da
  - 521e7ecd-e5b1-4f99-a1cd-3d67091be618
  - cf3f7e19-1eb6-4800-b0ad-827db191a1a7
  - 51c3d210-e82d-44b9-b32e-ca6e1f381232
  - 60b31668-8f57-48b9-8852-44130da97442
  - bcfa335f-78c0-42f1-a1fe-a7bb7ababf51
  - c94bde92-2a59-4873-ae34-17c9946f541c
  - 02632c57-949d-45c8-8e72-c08e28438cf6
  - 9cfa6ef6-5862-4077-9449-df15979317e2
  - d077d2a2-e4ba-4c3a-8941-79c998176af5
  - 479265f5-cb91-4d28-b803-40c20da84d2b
  - e9b09ca9-7812-44ce-b1ad-e0a15ee7d493
  - 82b8f99d-6c51-414e-8cd7-69b0593ff148
  - 3934db88-15f4-4c59-addd-f70b4c2b90e0
  - 771f23d6-9f1f-42fc-950e-36523c14573b
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638792345
---
