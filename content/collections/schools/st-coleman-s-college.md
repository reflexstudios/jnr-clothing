---
id: 5a7d01a0-6d6c-4beb-9cac-1177d193cb07
blueprint: schools
school_logo: st-colmans.jpg
school_type: secondary
title: 'ST COLEMAN''S COLLEGE'
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638370659
products:
  - a486a21f-aacc-4eba-91e9-5e97e223120e
  - 1ef70579-c6ad-4a99-8f4a-a77d3c35ec90
  - 8c6f7964-060f-4c27-8508-f9f545d60183
  - 8414a306-4656-41f2-b004-180effe7b23a
  - 9895394f-7cd3-466d-8267-9f8ae1d403ab
  - 32c539d8-3ca1-409d-8509-2413a1c01c92
---
