---
id: ab39bfd2-7a27-40e2-9b53-a66283245169
blueprint: schools
school_logo: 'Bunscoil-Naomh-Proinsias,-Lurgan-min.JPG'
school_type: primary
title: 'BUNSCOIL NAOMH PROINSIAS LURGAN'
products:
  - 30b1317f-2010-471b-99e1-3c5e4a01fcbc
  - 1b0ca9ce-4306-47e6-8431-d18b48ae93c5
  - 8c6f7964-060f-4c27-8508-f9f545d60183
  - 3bfcc30f-6465-49af-b454-aed168021368
  - 5332fd74-82e7-44f3-b416-eb35e7829e68
  - 5b3dd412-e23a-4eea-a627-d062870397da
  - 26befb4b-8b04-48e3-874a-4ba9e81a528c
  - e08a086b-c988-4f01-af6a-567260d8822e
  - bba4b097-f85f-4b93-b486-63a0f60ee640
  - 95099e98-766d-490a-a340-497c7130056c
  - ed86c7fa-422b-4975-90ae-67537ea9de51
  - 9d2a9354-312b-4331-bd92-235cfe61394e
  - 603518ff-329d-4c11-ba12-cae279ae5a62
  - 9b479632-acd5-42f0-b102-5a851c1eea02
  - a5080280-95f1-427f-976f-fca181150271
  - 521e7ecd-e5b1-4f99-a1cd-3d67091be618
  - f8991fa2-7e47-40d4-b487-a41362432bb8
  - 4a224712-d2d4-4af9-bc8d-4c59bd270d97
  - f5298acb-88d9-43c7-9217-e7acba6a73f4
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638791608
---
