---
id: 27d76421-cf81-448c-9d84-bfc19a9ff5ed
blueprint: schools
school_logo: st-patricks-Aghacommon-min.JPG
school_type: primary
title: 'ST PATRICK''S AGHACOMMON'
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638792294
products:
  - 56bf0924-0317-4324-b71f-24f006d7f57b
  - 34fb12ec-c993-4fd4-94fb-2ef740808b4d
  - a8987176-b096-4854-883f-1e1d641e0b2b
  - 188bf702-1471-4e5c-9c53-8c8a98bddb9c
  - 8fc9bc55-41cc-40f0-bef5-8c4045ebabd1
  - cf3f7e19-1eb6-4800-b0ad-827db191a1a7
  - 51c3d210-e82d-44b9-b32e-ca6e1f381232
  - 534b6f39-47df-4a43-9ece-e1342934d378
  - 157b453c-0ec3-4b93-8bd6-54e9a7a4e382
  - 6416f9c8-5d78-4fcb-a218-244272412b09
  - fccfd3a6-9d15-4dee-a9e3-7549f3e34bb5
  - 521e7ecd-e5b1-4f99-a1cd-3d67091be618
  - 60b31668-8f57-48b9-8852-44130da97442
  - e9b09ca9-7812-44ce-b1ad-e0a15ee7d493
  - c8fb9211-a185-482d-a7fb-dd39cdef27d5
  - ff016c01-061b-44a7-9e9f-e44f8cfa5ff1
  - 06a9a0ec-bf90-4d87-88ea-63b434db9645
  - e61c553d-0ec3-4ff4-b64b-27bd8e864068
  - 5332fd74-82e7-44f3-b416-eb35e7829e68
  - 5b3dd412-e23a-4eea-a627-d062870397da
---
