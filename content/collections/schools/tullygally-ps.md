---
id: b796837f-9226-4706-81a5-9ff345f0818b
blueprint: schools
school_logo: tullygally-sweatshirt-min.JPG
school_type: primary
title: 'TULLYGALLY PS'
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638794944
products:
  - 9bdd7dae-d227-40aa-b6b7-df11793b231b
  - a5080280-95f1-427f-976f-fca181150271
  - fccfd3a6-9d15-4dee-a9e3-7549f3e34bb5
  - 521e7ecd-e5b1-4f99-a1cd-3d67091be618
  - f8991fa2-7e47-40d4-b487-a41362432bb8
  - 60b31668-8f57-48b9-8852-44130da97442
  - e9b09ca9-7812-44ce-b1ad-e0a15ee7d493
  - 56bf0924-0317-4324-b71f-24f006d7f57b
  - 34fb12ec-c993-4fd4-94fb-2ef740808b4d
  - 74ab7adc-b36e-498e-ab9b-5e5dcb8fb3f6
  - 51c3d210-e82d-44b9-b32e-ca6e1f381232
  - c11c0747-6a92-4a30-a7e8-381be84e4442
  - 603518ff-329d-4c11-ba12-cae279ae5a62
  - 9b479632-acd5-42f0-b102-5a851c1eea02
  - cf3f7e19-1eb6-4800-b0ad-827db191a1a7
  - 534b6f39-47df-4a43-9ece-e1342934d378
  - 157b453c-0ec3-4b93-8bd6-54e9a7a4e382
  - e61c553d-0ec3-4ff4-b64b-27bd8e864068
  - 5332fd74-82e7-44f3-b416-eb35e7829e68
  - 5b3dd412-e23a-4eea-a627-d062870397da
  - 3bfcc30f-6465-49af-b454-aed168021368
---
