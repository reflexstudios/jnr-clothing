---
id: 8fe0fa07-1e23-40cc-a266-2fde240fb62f
blueprint: schools
school_logo: Portadown-Intergrated-Primary-School---348076-min.JPG
school_type: primary
title: 'Portadown Integrated'
products:
  - fccfd3a6-9d15-4dee-a9e3-7549f3e34bb5
  - 521e7ecd-e5b1-4f99-a1cd-3d67091be618
  - 479265f5-cb91-4d28-b803-40c20da84d2b
  - 60b31668-8f57-48b9-8852-44130da97442
  - 6fb25985-d718-469f-b701-ba4a04b01740
  - 1cbf63d9-5ebc-42c0-a3e0-739b203feac8
  - 12b09bb6-290a-496b-80fe-925b06a44ef9
  - e9b09ca9-7812-44ce-b1ad-e0a15ee7d493
  - a3819dc7-665c-49a5-825d-d03acf849f2d
  - 5ae4a55e-fde9-40ce-9bca-2224e169fd9a
  - 56bf0924-0317-4324-b71f-24f006d7f57b
  - 34fb12ec-c993-4fd4-94fb-2ef740808b4d
  - cf3f7e19-1eb6-4800-b0ad-827db191a1a7
  - 51c3d210-e82d-44b9-b32e-ca6e1f381232
  - 534b6f39-47df-4a43-9ece-e1342934d378
  - 157b453c-0ec3-4b93-8bd6-54e9a7a4e382
  - e61c553d-0ec3-4ff4-b64b-27bd8e864068
  - d077d2a2-e4ba-4c3a-8941-79c998176af5
  - 5332fd74-82e7-44f3-b416-eb35e7829e68
  - 5b3dd412-e23a-4eea-a627-d062870397da
  - bbade44c-0ab3-495e-8154-da9cce390b84
  - 078312a3-c735-4d76-8bc1-dd3deefe698d
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638449281
---
