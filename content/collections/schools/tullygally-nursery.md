---
id: 9710ddee-7c73-4020-b1e3-57ca33b0584b
blueprint: schools
school_logo: tullygally-min.JPG
school_type: pre-school
title: 'TULLYGALLY NURSERY'
products:
  - 81e3bb2b-1213-4507-b0e2-589c8a140bf4
  - 0c587fa9-bfa8-4920-8701-4cb465b33ded
  - ca88971c-0e8e-47ad-9985-5f1f38e50805
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638794547
---
