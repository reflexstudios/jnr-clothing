---
id: bb648b37-25ea-4920-805f-7fc7d4d37a3e
blueprint: schools
school_logo: st_johns-BUNSCOIL-min.JPG
school_type: primary
title: 'ST JOHN THE BAPTIST PS'
products:
  - 74ab7adc-b36e-498e-ab9b-5e5dcb8fb3f6
  - c11c0747-6a92-4a30-a7e8-381be84e4442
  - 603518ff-329d-4c11-ba12-cae279ae5a62
  - 9b479632-acd5-42f0-b102-5a851c1eea02
  - 6fb25985-d718-469f-b701-ba4a04b01740
  - aae9733c-d28d-4d80-a6a5-4c37a4b5e906
  - 51ff0161-eaba-4f8d-a359-c4e484192d99
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638794177
---
