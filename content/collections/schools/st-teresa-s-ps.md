---
id: 81a32fbd-6564-447b-880c-dd977bdc2d06
blueprint: schools
school_logo: st-teresas-min.JPG
school_type: primary
title: 'ST TERESA''S PS'
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638794998
products:
  - 521e7ecd-e5b1-4f99-a1cd-3d67091be618
  - 60b31668-8f57-48b9-8852-44130da97442
  - 6fb25985-d718-469f-b701-ba4a04b01740
  - e9b09ca9-7812-44ce-b1ad-e0a15ee7d493
  - fccfd3a6-9d15-4dee-a9e3-7549f3e34bb5
  - 3e04fbcb-8848-42fa-98b8-3bcefa817658
  - e6a8ae7e-0f37-406c-ada0-3396474d5b95
  - 12b09bb6-290a-496b-80fe-925b06a44ef9
  - 860f8f0f-000e-410a-a43e-3a775d6debfc
  - 76c34723-fab7-4fc1-b3c6-e0e4799e1e2a
  - 423ae112-27bc-4acf-9c45-1b2e5a614203
  - f40db013-a1a7-4301-ac97-e87554d6c4cb
  - 31451611-d7e7-49f1-9f11-9ae0a01e37a5
  - 56bf0924-0317-4324-b71f-24f006d7f57b
  - 34fb12ec-c993-4fd4-94fb-2ef740808b4d
  - cf3f7e19-1eb6-4800-b0ad-827db191a1a7
  - 51c3d210-e82d-44b9-b32e-ca6e1f381232
  - 534b6f39-47df-4a43-9ece-e1342934d378
  - 157b453c-0ec3-4b93-8bd6-54e9a7a4e382
  - e61c553d-0ec3-4ff4-b64b-27bd8e864068
  - 5b3dd412-e23a-4eea-a627-d062870397da
  - 5332fd74-82e7-44f3-b416-eb35e7829e68
---
