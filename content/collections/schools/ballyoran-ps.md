---
id: a30bd3f5-2060-4e36-8d36-e01d8b9b099c
blueprint: schools
school_logo: ballyoran-primary-school-min.JPG
school_type: primary
title: 'BALLYORAN PS'
products:
  - 56bf0924-0317-4324-b71f-24f006d7f57b
  - 34fb12ec-c993-4fd4-94fb-2ef740808b4d
  - 1b0ca9ce-4306-47e6-8431-d18b48ae93c5
  - 8c6f7964-060f-4c27-8508-f9f545d60183
  - 0b6786b9-1976-4b40-99c0-34eb54f7b7bf
  - 88d260c8-79eb-4879-8d8f-e1cd86481bed
  - cf3f7e19-1eb6-4800-b0ad-827db191a1a7
  - 51c3d210-e82d-44b9-b32e-ca6e1f381232
  - 534b6f39-47df-4a43-9ece-e1342934d378
  - 157b453c-0ec3-4b93-8bd6-54e9a7a4e382
  - e9b09ca9-7812-44ce-b1ad-e0a15ee7d493
  - 76c34723-fab7-4fc1-b3c6-e0e4799e1e2a
  - e61c553d-0ec3-4ff4-b64b-27bd8e864068
  - 5332fd74-82e7-44f3-b416-eb35e7829e68
  - 5b3dd412-e23a-4eea-a627-d062870397da
  - fccfd3a6-9d15-4dee-a9e3-7549f3e34bb5
  - 521e7ecd-e5b1-4f99-a1cd-3d67091be618
  - 60b31668-8f57-48b9-8852-44130da97442
updated_by: 4bfa4be6-8be8-40f2-859e-bf8cbe8ba010
updated_at: 1639569655
---
