---
id: a4245daa-bf18-4162-8671-da84e547a2ee
blueprint: schools
school_logo: drumgor-min.JPG
school_type: primary
title: DRUMGOR
products:
  - 56bf0924-0317-4324-b71f-24f006d7f57b
  - 34fb12ec-c993-4fd4-94fb-2ef740808b4d
  - d593d52e-27f5-466e-90ed-339170ceb56a
  - 2fe2ecee-cf06-45a4-b96d-b2387633a9e2
  - a8987176-b096-4854-883f-1e1d641e0b2b
  - cf3f7e19-1eb6-4800-b0ad-827db191a1a7
  - 51c3d210-e82d-44b9-b32e-ca6e1f381232
  - 534b6f39-47df-4a43-9ece-e1342934d378
  - 157b453c-0ec3-4b93-8bd6-54e9a7a4e382
  - 6fec5917-be41-4ea4-bffb-f80220dad2e2
  - 7327315e-171b-4d0a-a3e8-f69f177ddd10
  - 48abeeba-94dc-40e0-922f-b80bb0eedcb9
  - 9a9eb1fc-f213-49f0-8dd5-04d1cc57cea2
  - e61c553d-0ec3-4ff4-b64b-27bd8e864068
  - 5332fd74-82e7-44f3-b416-eb35e7829e68
  - 5b3dd412-e23a-4eea-a627-d062870397da
  - fccfd3a6-9d15-4dee-a9e3-7549f3e34bb5
  - 521e7ecd-e5b1-4f99-a1cd-3d67091be618
  - ff016c01-061b-44a7-9e9f-e44f8cfa5ff1
  - 60b31668-8f57-48b9-8852-44130da97442
  - e9b09ca9-7812-44ce-b1ad-e0a15ee7d493
  - 6fb25985-d718-469f-b701-ba4a04b01740
  - aae9733c-d28d-4d80-a6a5-4c37a4b5e906
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638789101
---
