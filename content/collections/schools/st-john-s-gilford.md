---
id: 6522014f-0687-4c4d-8827-bf09e3a3040d
blueprint: schools
school_logo: ST-JOHNS-PS-Gilford-min.JPG
school_type: primary
title: 'ST JOHN''S GILFORD'
products:
  - e61c553d-0ec3-4ff4-b64b-27bd8e864068
  - fccfd3a6-9d15-4dee-a9e3-7549f3e34bb5
  - 60b31668-8f57-48b9-8852-44130da97442
  - 6fb25985-d718-469f-b701-ba4a04b01740
  - aae9733c-d28d-4d80-a6a5-4c37a4b5e906
  - d077d2a2-e4ba-4c3a-8941-79c998176af5
  - 8229bb52-1c9b-4a95-bc49-153d5e2c8956
  - 02632c57-949d-45c8-8e72-c08e28438cf6
  - bbade44c-0ab3-495e-8154-da9cce390b84
  - 078312a3-c735-4d76-8bc1-dd3deefe698d
  - 479265f5-cb91-4d28-b803-40c20da84d2b
  - 82b8f99d-6c51-414e-8cd7-69b0593ff148
  - 3934db88-15f4-4c59-addd-f70b4c2b90e0
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638447522
---
