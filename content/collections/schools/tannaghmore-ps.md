---
id: fee34e15-4ecc-4dfd-b492-f9f2c58fae6d
blueprint: schools
school_logo: tannaghmore-ps-min.JPG
school_type: primary
title: 'TANNAGHMORE PS'
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638794968
products:
  - 534b6f39-47df-4a43-9ece-e1342934d378
  - 157b453c-0ec3-4b93-8bd6-54e9a7a4e382
  - 06a9a0ec-bf90-4d87-88ea-63b434db9645
  - e61c553d-0ec3-4ff4-b64b-27bd8e864068
  - 60b31668-8f57-48b9-8852-44130da97442
  - e9b09ca9-7812-44ce-b1ad-e0a15ee7d493
  - 6416f9c8-5d78-4fcb-a218-244272412b09
  - fccfd3a6-9d15-4dee-a9e3-7549f3e34bb5
  - c8fb9211-a185-482d-a7fb-dd39cdef27d5
  - ee6f5392-8879-4161-b821-f2ca6033a226
  - 56bf0924-0317-4324-b71f-24f006d7f57b
  - 34fb12ec-c993-4fd4-94fb-2ef740808b4d
  - cf3f7e19-1eb6-4800-b0ad-827db191a1a7
  - 51c3d210-e82d-44b9-b32e-ca6e1f381232
  - 297fc9a4-fab9-4049-afb6-67c41f71a657
  - a8987176-b096-4854-883f-1e1d641e0b2b
  - 188bf702-1471-4e5c-9c53-8c8a98bddb9c
  - 2fe2ecee-cf06-45a4-b96d-b2387633a9e2
  - aa4a8240-336f-4d8f-9d30-31e1db1d05ea
  - 8fc9bc55-41cc-40f0-bef5-8c4045ebabd1
  - d5dd79e4-89ee-41af-be23-37df554e1935
  - 5dd3f4d1-7ac6-432a-91aa-1998d3d17a71
  - 9a0d99ed-fc62-4bf6-9bc1-b2f398fe61b8
  - 94b66615-4df0-4364-bbcc-efb1060f8b1e
---
