---
id: 4e8686dd-d120-42ff-aa30-4f5290fc0230
blueprint: schools
school_logo: ceara-full-crest-min.JPG
school_type: primary
title: 'CEARA SCHOOL LURGAN'
products:
  - 56bf0924-0317-4324-b71f-24f006d7f57b
  - 34fb12ec-c993-4fd4-94fb-2ef740808b4d
  - cf3f7e19-1eb6-4800-b0ad-827db191a1a7
  - 534b6f39-47df-4a43-9ece-e1342934d378
  - 157b453c-0ec3-4b93-8bd6-54e9a7a4e382
  - be05d080-42a1-464c-a3bd-2b87d7ac8fcb
  - e9b09ca9-7812-44ce-b1ad-e0a15ee7d493
  - 6fb25985-d718-469f-b701-ba4a04b01740
  - aae9733c-d28d-4d80-a6a5-4c37a4b5e906
  - eb99287d-dd8d-4fa6-bb09-1740ef3320df
  - 7c2130fa-0523-4237-bd30-d8b3bc2f1935
  - c94bde92-2a59-4873-ae34-17c9946f541c
  - 9a9eb1fc-f213-49f0-8dd5-04d1cc57cea2
  - 9cfa6ef6-5862-4077-9449-df15979317e2
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638788232
---
