---
id: 65ae0d76-9eeb-4b5e-a60f-918bd9f7c7ea
blueprint: schools
school_logo: tannaghmore-gardens-nursery-min.JPG
school_type: pre-school
title: 'TANNAGHMORE GARDENS PLAYGROUP'
products:
  - 1c9b1d8a-3cdc-4f97-834d-e96b79d04368
  - 806638ac-9974-41d1-bdf1-171d5f098c8c
  - 80043099-3532-48e7-9216-b1a758a023d1
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638794829
---
