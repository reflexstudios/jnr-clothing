---
id: 136ee58e-c3c2-45cb-b457-83b200936ff7
blueprint: schools
school_logo: st-marys-derrytrasna-min.JPG
school_type: primary
title: 'ST MARY''S DERRYTRASNA'
products:
  - ed86c7fa-422b-4975-90ae-67537ea9de51
  - 4a224712-d2d4-4af9-bc8d-4c59bd270d97
  - f5298acb-88d9-43c7-9217-e7acba6a73f4
  - fccfd3a6-9d15-4dee-a9e3-7549f3e34bb5
  - 521e7ecd-e5b1-4f99-a1cd-3d67091be618
  - 60b31668-8f57-48b9-8852-44130da97442
  - 12b09bb6-290a-496b-80fe-925b06a44ef9
  - 486d5df8-2072-4218-a827-d7f3a1ac38a4
updated_by: 2ba0b563-6a6e-47ed-a9c4-0bf57c5ad969
updated_at: 1638793916
---
