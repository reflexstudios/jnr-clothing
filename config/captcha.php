<?php

return [
    'service' => 'Recaptcha', // options: Recaptcha / Hcaptcha
    'sitekey' => env('CAPTCHA_SITEKEY', '6Lff0MAeAAAAAPx5Iy2MWU38nlC2zZFC_fL-MO_d'),
    'secret' => env('CAPTCHA_SECRET', '6Lff0MAeAAAAAPsS2toNJEmHPPoGAZaXnxH75nNB'),
    'collections' => [],
    'forms' => ['quote_form', 'contact_form', 'product_form'],
    'user_login' => false,
    'user_registration' => false,
    'error_message' => 'Captcha failed.  Please click the "I am not a robot" button',
    'disclaimer' => '',
    'invisible' => true,
    'hide_badge' => false,
    'enable_api_routes' => false,
];
