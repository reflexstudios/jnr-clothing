# JnR Clothing 
DEV: 

username: hello@reflex-studios.com
password: reflexstudios

## Setup
- git clone git clone git@bitbucket.org:reflexstudios/jnr-clothing.git
- cd /sitename
- composer install
- cp .env.example .env && php artisan key:generate

## Valet:
- valet link
- valet park
- valet open
## Dev:
- npm install
- npm run dev

## Important Links

- [Statamic Main Site](https://statamic.com)
- [Statamic 3 Documentation][docs]
- [Statamic 3 Core Package Repo][cms-repo]
- [Statamic 3 Migrator](https://github.com/statamic/migrator)
- [Statamic Discord][discord]

[docs]: https://statamic.dev/
[discord]: https://statamic.com/discord
[contribution]: https://github.com/statamic/cms/blob/master/CONTRIBUTING.md
[cms-repo]: https://github.com/statamic/cms
