<session-expiry
    email="<?php echo e($email); ?>"
    :warn-at="<?php echo e($warnAt); ?>"
    :lifetime="<?php echo e($lifetime); ?>"
    :oauth-provider="<?php echo e(json_encode($oauth)); ?>"
></session-expiry><?php /**PATH /Users/samdonaghybell/Dev/jnr-clothing/vendor/statamic/cms/src/Providers/../../resources/views/partials/session-expiry.blade.php ENDPATH**/ ?>